#!/bin/bash

cd /usr/local/iiat/job-manager
rm -rf node_modules
npm i

cd ../web-api
rm -rf node_modules
npm i
