#!/bin/bash

CONTAINER_NAME="happy_rubin"

cd job-manager
tsc
cd ../web-api
tsc
cd ..

docker cp bootstrap.sh $CONTAINER_NAME:/usr/local/iiat/
docker cp reinstallNodeModules.sh $CONTAINER_NAME:/usr/local/iiat/
docker cp job-manager $CONTAINER_NAME:/usr/local/iiat/
docker cp web-api $CONTAINER_NAME:/usr/local/iiat/

docker exec $CONTAINER_NAME /usr/local/iiat/reinstallNodeModules.sh
docker exec $CONTAINER_NAME /usr/local/iiat/bootstrap.sh


