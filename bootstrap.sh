#!/bin/bash
set -m

export IIAT_WORKING_DIR="/var/local/iiat"
export SERVER_PORT=8080
export COOKIE_SECRET="2kkd0wk3thokdw"

mkdir $IIAT_WORKING_DIR

# Kill server-cluster if it is already running.
if [ -f "server-cluster.pid" ] ; then
    SC_PID=`cat server-cluster.pid`
    echo "Server cluster is already running. Killing pid=${SC_PID}."
    kill $SC_PID
fi

# Kill job-manager if it is already running.
if [ -f "job-manager.pid" ] ; then
    JM_PID=`cat job-manager.pid`
    echo "Job manager is already running. Killing pid=${JM_PID}."
    kill $JM_PID
fi

node /usr/local/iiat/web-api/bin/server-cluster.js >> web-api.log &

API_PROC=$!
# Put the server cluster pid into a file 
echo $API_PROC > server-cluster.pid

node /usr/local/iiat/job-manager/bin/job-manager.js >> job-manager.log &

JOB_PROC=$!
# Put the job manager pid into a file
echo $JOB_PROC > job-manager.pid

tail -f web-api.log job-manager.log

