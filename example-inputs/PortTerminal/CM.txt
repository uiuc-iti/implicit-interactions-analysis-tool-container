begin AGENT where

	CM := (READ ; CARGO) + (SEQ ; SERVE) + (UPDT ; OPER)
	
end


begin NEXT_BEHAVIOUR where
	
	(arrive, READ)   = READ
	(mnge1, READ)    = READ
	(mnge2, READ)    = READ
	(ship1, READ)    = READ
	(ship2, READ)    = READ
	(crane1, READ)   = READ
	(crane2, READ)   = READ
	(allocd, READ)   = READ
	(berth, READ)    = READ
	(dock, READ)     = READ
	(oper1, READ)    = READ
	(oper2, READ)    = READ
	(carrier, READ)  = READ
	(assgnd, READ)   = SEQ
	(serve, READ)    = READ
	(served, READ)   = UPDT
	(done, READ)     = READ
	(compl1, READ)   = READ
	(compl2, READ)   = READ
	(deprt1, READ)   = READ
	(deprt2, READ)   = READ
	
	(arrive, CARGO)   = CARGO
	(mnge1, CARGO)    = CARGO
	(mnge2, CARGO)    = CARGO
	(ship1, CARGO)    = CARGO
	(ship2, CARGO)    = CARGO
	(crane1, CARGO)   = CARGO
	(crane2, CARGO)   = CARGO
	(allocd, CARGO)   = CARGO
	(berth, CARGO)    = CARGO
	(dock, CARGO)     = CARGO
	(oper1, CARGO)    = CARGO
	(oper2, CARGO)    = CARGO
	(carrier, CARGO)  = CARGO
	(assgnd, CARGO)   = SERVE
	(serve, CARGO)    = CARGO
	(served, CARGO)   = OPER
	(done, CARGO)     = CARGO
	(compl1, CARGO)   = CARGO
	(compl2, CARGO)   = CARGO
	(deprt1, CARGO)   = CARGO
	(deprt2, CARGO)   = CARGO
	
	(arrive, SEQ)   = SEQ
	(mnge1, SEQ)    = SEQ
	(mnge2, SEQ)    = SEQ
	(ship1, SEQ)    = SEQ
	(ship2, SEQ)    = SEQ
	(crane1, SEQ)   = SEQ
	(crane2, SEQ)   = SEQ
	(allocd, SEQ)   = SEQ
	(berth, SEQ)    = SEQ
	(dock, SEQ)     = SEQ
	(oper1, SEQ)    = READ
	(oper2, SEQ)    = READ
	(carrier, SEQ)  = SEQ
	(assgnd, SEQ)   = SEQ
	(serve, SEQ)    = SEQ
	(served, SEQ)   = UPDT
	(done, SEQ)     = SEQ
	(compl1, SEQ)   = SEQ
	(compl2, SEQ)   = SEQ
	(deprt1, SEQ)   = SEQ
	(deprt2, SEQ)   = SEQ
	
	(arrive, SERVE)   = SERVE
	(mnge1, SERVE)    = SERVE
	(mnge2, SERVE)    = SERVE
	(ship1, SERVE)    = SERVE
	(ship2, SERVE)    = SERVE
	(crane1, SERVE)   = SERVE
	(crane2, SERVE)   = SERVE
	(allocd, SERVE)   = SERVE
	(berth, SERVE)    = SERVE
	(dock, SERVE)     = SERVE
	(oper1, SERVE)    = CARGO
	(oper2, SERVE)    = CARGO
	(carrier, SERVE)  = SERVE
	(assgnd, SERVE)   = SERVE
	(serve, SERVE)    = SERVE
	(served, SERVE)   = OPER
	(done, SERVE)     = SERVE
	(compl1, SERVE)   = SERVE
	(compl2, SERVE)   = SERVE
	(deprt1, SERVE)   = SERVE
	(deprt2, SERVE)   = SERVE
	
	(arrive, UPDT)   = UPDT
	(mnge1, UPDT)    = UPDT
	(mnge2, UPDT)    = UPDT
	(ship1, UPDT)    = UPDT
	(ship2, UPDT)    = UPDT
	(crane1, UPDT)   = UPDT
	(crane2, UPDT)   = UPDT
	(allocd, UPDT)   = UPDT
	(berth, UPDT)    = UPDT
	(dock, UPDT)     = UPDT
	(oper1, UPDT)    = READ
	(oper2, UPDT)    = READ
	(carrier, UPDT)  = UPDT
	(assgnd, UPDT)   = SEQ
	(serve, UPDT)    = UPDT
	(served, UPDT)   = UPDT
	(done, UPDT)     = UPDT
	(compl1, UPDT)   = UPDT
	(compl2, UPDT)   = UPDT
	(deprt1, UPDT)   = UPDT
	(deprt2, UPDT)   = UPDT
	
	(arrive, OPER)   = OPER
	(mnge1, OPER)    = OPER
	(mnge2, OPER)    = OPER
	(ship1, OPER)    = OPER
	(ship2, OPER)    = OPER
	(crane1, OPER)   = OPER
	(crane2, OPER)   = OPER
	(allocd, OPER)   = OPER
	(berth, OPER)    = OPER
	(dock, OPER)     = OPER
	(oper1, OPER)    = CARGO
	(oper2, OPER)    = CARGO
	(carrier, OPER)  = OPER
	(assgnd, OPER)   = SERVE
	(serve, OPER)    = OPER
	(served, OPER)   = OPER
	(done, OPER)     = OPER
	(compl1, OPER)   = OPER
	(compl2, OPER)   = OPER
	(deprt1, OPER)   = OPER
	(deprt2, OPER)   = OPER

end


begin NEXT_STIMULUS where

	(arrive, READ)   = N
	(mnge1, READ)    = N
	(mnge2, READ)    = N
	(ship1, READ)    = N
	(ship2, READ)    = N
	(crane1, READ)   = N
	(crane2, READ)   = N
	(allocd, READ)   = N
	(berth, READ)    = N
	(dock, READ)     = N
	(oper1, READ)    = N
	(oper2, READ)    = N
	(carrier, READ)  = N
	(assgnd, READ)   = assgnd
	(serve, READ)    = N
	(served, READ)   = served
	(done, READ)     = N
	(compl1, READ)   = N
	(compl2, READ)   = N
	(deprt1, READ)   = N
	(deprt2, READ)   = N
	
	(arrive, CARGO)   = N
	(mnge1, CARGO)    = N
	(mnge2, CARGO)    = N
	(ship1, CARGO)    = N
	(ship2, CARGO)    = N
	(crane1, CARGO)   = N
	(crane2, CARGO)   = N
	(allocd, CARGO)   = N
	(berth, CARGO)    = N
	(dock, CARGO)     = N
	(oper1, CARGO)    = N
	(oper2, CARGO)    = N
	(carrier, CARGO)  = N
	(assgnd, CARGO)   = serve
	(serve, CARGO)    = N
	(served, CARGO)   = done
	(done, CARGO)     = N
	(compl1, CARGO)   = N
	(compl2, CARGO)   = N
	(deprt1, CARGO)   = N
	(deprt2, CARGO)   = N
	
	(arrive, SEQ)   = N
	(mnge1, SEQ)    = N
	(mnge2, SEQ)    = N
	(ship1, SEQ)    = N
	(ship2, SEQ)    = N
	(crane1, SEQ)   = N
	(crane2, SEQ)   = N
	(allocd, SEQ)   = N
	(berth, SEQ)    = N
	(dock, SEQ)     = N
	(oper1, SEQ)    = oper1
	(oper2, SEQ)    = oper2
	(carrier, SEQ)  = N
	(assgnd, SEQ)   = N
	(serve, SEQ)    = N
	(served, SEQ)   = served
	(done, SEQ)     = N
	(compl1, SEQ)   = N
	(compl2, SEQ)   = N
	(deprt1, SEQ)   = N
	(deprt2, SEQ)   = N
	
	(arrive, SERVE)   = N
	(mnge1, SERVE)    = N
	(mnge2, SERVE)    = N
	(ship1, SERVE)    = N
	(ship2, SERVE)    = N
	(crane1, SERVE)   = N
	(crane2, SERVE)   = N
	(allocd, SERVE)   = N
	(berth, SERVE)    = N
	(dock, SERVE)     = N
	(oper1, SERVE)    = carrier
	(oper2, SERVE)    = carrier
	(carrier, SERVE)  = N
	(assgnd, SERVE)   = N
	(serve, SERVE)    = N
	(served, SERVE)   = done
	(done, SERVE)     = N
	(compl1, SERVE)   = N
	(compl2, SERVE)   = N
	(deprt1, SERVE)   = N
	(deprt2, SERVE)   = N
	
	(arrive, UPDT)   = N
	(mnge1, UPDT)    = N
	(mnge2, UPDT)    = N
	(ship1, UPDT)    = N
	(ship2, UPDT)    = N
	(crane1, UPDT)   = N
	(crane2, UPDT)   = N
	(allocd, UPDT)   = N
	(berth, UPDT)    = N
	(dock, UPDT)     = N
	(oper1, UPDT)    = oper1
	(oper2, UPDT)    = oper2
	(carrier, UPDT)  = N
	(assgnd, UPDT)   = assgnd
	(serve, UPDT)    = N
	(served, UPDT)   = N
	(done, UPDT)     = N
	(compl1, UPDT)   = N
	(compl2, UPDT)   = N
	(deprt1, UPDT)   = N
	(deprt2, UPDT)   = N
	
	(arrive, OPER)   = N
	(mnge1, OPER)    = N
	(mnge2, OPER)    = N
	(ship1, OPER)    = N
	(ship2, OPER)    = N
	(crane1, OPER)   = N
	(crane2, OPER)   = N
	(allocd, OPER)   = N
	(berth, OPER)    = N
	(dock, OPER)     = N
	(oper1, OPER)    = carrier
	(oper2, OPER)    = carrier
	(carrier, OPER)  = N
	(assgnd, OPER)   = serve
	(serve, OPER)    = N
	(served, OPER)   = N
	(done, OPER)     = N
	(compl1, OPER)   = N
	(compl2, OPER)   = N
	(deprt1, OPER)   = N
	(deprt2, OPER)   = N
	
end


begin CONCRETE_BEHAVIOUR where

	READ  => [ receive y;
	          if (y >= OPER1) -> plan := bayPlanA
			   | (y >= OPER2) -> plan := bayPlanB
		      fi ]	
	
	CARGO => [ containers := plan ]
	
	SEQ   => [ sequence := carrierAssign ]
	
	SERVE => [ position := sequence ]
	
	UPDT  => [ plan := carrierState ]
	
	OPER  => [ operation := plan ]	
	
end