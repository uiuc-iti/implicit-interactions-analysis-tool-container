begin AGENT where

	SMb := SRVTb + POSNb + LEAVEb 
	
end


begin NEXT_BEHAVIOUR where
	
	(arrive, SRVTb)   = SRVTb
	(mnge1, SRVTb)    = SRVTb
	(mnge2, SRVTb)    = SRVTb
	(ship1, SRVTb)    = SRVTb
	(ship2, SRVTb)    = SRVTb
	(crane1, SRVTb)   = SRVTb
	(crane2, SRVTb)   = SRVTb
	(allocd, SRVTb)   = SRVTb
	(berth, SRVTb)    = POSNb
	(dock, SRVTb)     = SRVTb
	(oper1, SRVTb)    = SRVTb
	(oper2, SRVTb)    = SRVTb
	(carrier, SRVTb)  = SRVTb
	(assgnd, SRVTb)   = SRVTb
	(serve, SRVTb)    = SRVTb
	(served, SRVTb)   = SRVTb
	(done, SRVTb)     = SRVTb
	(compl1, SRVTb)   = SRVTb
	(compl2, SRVTb)   = SRVTb
	(deprt1, SRVTb)   = SRVTb
	(deprt2, SRVTb)   = SRVTb
	
	(arrive, POSNb)   = POSNb
	(mnge1, POSNb)    = POSNb
	(mnge2, POSNb)    = POSNb
	(ship1, POSNb)    = POSNb
	(ship2, POSNb)    = POSNb
	(crane1, POSNb)   = POSNb
	(crane2, POSNb)   = POSNb
	(allocd, POSNb)   = POSNb
	(berth, POSNb)    = POSNb
	(dock, POSNb)     = POSNb
	(oper1, POSNb)    = POSNb
	(oper2, POSNb)    = POSNb
	(carrier, POSNb)  = POSNb
	(assgnd, POSNb)   = POSNb
	(serve, POSNb)    = POSNb
	(served, POSNb)   = POSNb
	(done, POSNb)     = POSNb
	(compl1, POSNb)   = POSNb
	(compl2, POSNb)   = LEAVEb
	(deprt1, POSNb)   = POSNb
	(deprt2, POSNb)   = POSNb
	
	(arrive, LEAVEb)   = LEAVEb
	(mnge1, LEAVEb)    = LEAVEb
	(mnge2, LEAVEb)    = SRVTb
	(ship1, LEAVEb)    = LEAVEb
	(ship2, LEAVEb)    = LEAVEb
	(crane1, LEAVEb)   = LEAVEb
	(crane2, LEAVEb)   = LEAVEb
	(allocd, LEAVEb)   = LEAVEb
	(berth, LEAVEb)    = LEAVEb
	(dock, LEAVEb)     = LEAVEb
	(oper1, LEAVEb)    = LEAVEb
	(oper2, LEAVEb)    = LEAVEb
	(carrier, LEAVEb)  = LEAVEb
	(assgnd, LEAVEb)   = LEAVEb
	(serve, LEAVEb)    = LEAVEb
	(served, LEAVEb)   = LEAVEb
	(done, LEAVEb)     = LEAVEb
	(compl1, LEAVEb)   = LEAVEb
	(compl2, LEAVEb)   = LEAVEb
	(deprt1, LEAVEb)   = LEAVEb
	(deprt2, LEAVEb)   = LEAVEb

end


begin NEXT_STIMULUS where

	(arrive, SRVTb)   = N
	(mnge1, SRVTb)    = N
	(mnge2, SRVTb)    = N
	(ship1, SRVTb)    = N
	(ship2, SRVTb)    = N
	(crane1, SRVTb)   = N
	(crane2, SRVTb)   = N
	(allocd, SRVTb)   = N
	(berth, SRVTb)    = dock
	(dock, SRVTb)     = N
	(oper1, SRVTb)    = N
	(oper2, SRVTb)    = N
	(carrier, SRVTb)  = N
	(assgnd, SRVTb)   = N
	(serve, SRVTb)    = N
	(served, SRVTb)   = N
	(done, SRVTb)     = N
	(compl1, SRVTb)   = N
	(compl2, SRVTb)   = N
	(deprt1, SRVTb)   = N
	(deprt2, SRVTb)   = N
	
	(arrive, POSNb)   = N
	(mnge1, POSNb)    = N
	(mnge2, POSNb)    = N
	(ship1, POSNb)    = N
	(ship2, POSNb)    = N
	(crane1, POSNb)   = N
	(crane2, POSNb)   = N
	(allocd, POSNb)   = N
	(berth, POSNb)    = N
	(dock, POSNb)     = N
	(oper1, POSNb)    = N
	(oper2, POSNb)    = N
	(carrier, POSNb)  = N
	(assgnd, POSNb)   = N
	(serve, POSNb)    = N
	(served, POSNb)   = N
	(done, POSNb)     = N
	(compl1, POSNb)   = N
	(compl2, POSNb)   = deprt2
	(deprt1, POSNb)   = N
	(deprt2, POSNb)   = N
	
	(arrive, LEAVEb)   = N
	(mnge1, LEAVEb)    = N
	(mnge2, LEAVEb)    = ship2
	(ship1, LEAVEb)    = N
	(ship2, LEAVEb)    = N
	(crane1, LEAVEb)   = N
	(crane2, LEAVEb)   = N
	(allocd, LEAVEb)   = N
	(berth, LEAVEb)    = N
	(dock, LEAVEb)     = N
	(oper1, LEAVEb)    = N
	(oper2, LEAVEb)    = N
	(carrier, LEAVEb)  = N
	(assgnd, LEAVEb)   = N
	(serve, LEAVEb)    = N
	(served, LEAVEb)   = N
	(done, LEAVEb)     = N
	(compl1, LEAVEb)   = N
	(compl2, LEAVEb)   = N
	(deprt1, LEAVEb)   = N
	(deprt2, LEAVEb)   = N
	
end


begin CONCRETE_BEHAVIOUR where

	SRVTb  => [ serviceTB := departTB - arriveTB - waitTB ]
	
	POSNb  => [ dockPosB := berthPosB ]
	
	LEAVEb => [ dockPosB := NULL; serviceTB := 0 ]
	
end