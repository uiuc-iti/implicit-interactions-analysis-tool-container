begin AGENT where

	C := (cOPEN ; cON) + (cOFF ; cCLSD)

end


begin NEXT_BEHAVIOUR where
	
	(start,cOPEN)       = cOPEN
	(charge,cOPEN)      = cOPEN
	(charged,cOPEN)     = cOPEN
	(heat,cOPEN)        = cOPEN
	(heated,cOPEN)      = cOPEN
	(purgeN,cOPEN)      = cOPEN
	(purgeH,cOPEN)      = cOPEN
	(purged,cOPEN)      = cOPEN
	(catalyze,cOPEN)    = cOPEN
	(catalyzed,cOPEN)   = cOPEN
	(wash,cOPEN)        = cOPEN
	(washed,cOPEN)      = cOFF
	(raise,cOPEN)       = cOPEN
	(raised,cOPEN)      = cOPEN
	(vent,cOPEN)        = cOPEN
	(vented,cOPEN)      = cOPEN
	(transfer,cOPEN)    = cOPEN
	(transferred,cOPEN) = cOPEN
	(end,cOPEN)         = cOPEN
	
	(start,cON)       = cON
	(charge,cON)      = cON
	(charged,cON)     = cON
	(heat,cON)        = cON
	(heated,cON)      = cON
	(purgeN,cON)      = cON
	(purgeH,cON)      = cON
	(purged,cON)      = cON
	(catalyze,cON)    = cON
	(catalyzed,cON)   = cON
	(wash,cON)        = cON
	(washed,cON)      = cCLSD
	(raise,cON)       = cON
	(raised,cON)      = cON
	(vent,cON)        = cON
	(vented,cON)      = cON
	(transfer,cON)    = cON
	(transferred,cON) = cON
	(end,cON)         = cON
	
	(start,cOFF)       = cOFF
	(charge,cOFF)      = cOFF
	(charged,cOFF)     = cOFF
	(heat,cOFF)        = cOFF
	(heated,cOFF)      = cOFF
	(purgeN,cOFF)      = cOFF
	(purgeH,cOFF)      = cOFF
	(purged,cOFF)      = cOFF
	(catalyze,cOFF)    = cOPEN
	(catalyzed,cOFF)   = cOFF
	(wash,cOFF)        = cOFF
	(washed,cOFF)      = cOFF
	(raise,cOFF)       = cOFF
	(raised,cOFF)      = cOFF
	(vent,cOFF)        = cOFF
	(vented,cOFF)      = cOFF
	(transfer,cOFF)    = cOFF
	(transferred,cOFF) = cOFF
	(end,cOFF)         = cOFF
	
	(start,cCLSD)       = cCLSD
	(charge,cCLSD)      = cCLSD
	(charged,cCLSD)     = cCLSD
	(heat,cCLSD)        = cCLSD
	(heated,cCLSD)      = cCLSD
	(purgeN,cCLSD)      = cCLSD
	(purgeH,cCLSD)      = cCLSD
	(purged,cCLSD)      = cCLSD
	(catalyze,cCLSD)    = cON
	(catalyzed,cCLSD)   = cCLSD
	(wash,cCLSD)        = cCLSD
	(washed,cCLSD)      = cCLSD
	(raise,cCLSD)       = cCLSD
	(raised,cCLSD)      = cCLSD
	(vent,cCLSD)        = cCLSD
	(vented,cCLSD)      = cCLSD
	(transfer,cCLSD)    = cCLSD
	(transferred,cCLSD) = cCLSD
	(end,cCLSD)         = cCLSD
	

end


begin NEXT_STIMULUS where

	(start,cOPEN)       = N
	(charge,cOPEN)      = N
	(charged,cOPEN)     = N
	(heat,cOPEN)        = N
	(heated,cOPEN)      = N
	(purgeN,cOPEN)      = N
	(purgeH,cOPEN)      = N
	(purged,cOPEN)      = N
	(catalyze,cOPEN)    = catalyze
	(catalyzed,cOPEN)   = N
	(wash,cOPEN)        = N
	(washed,cOPEN)      = washed
	(raise,cOPEN)       = N
	(raised,cOPEN)      = N
	(vent,cOPEN)        = N
	(vented,cOPEN)      = N
	(transfer,cOPEN)    = N
	(transferred,cOPEN) = N
	(end,cOPEN)         = N
	
	(start,cON)       = N
	(charge,cON)      = N
	(charged,cON)     = N
	(heat,cON)        = N
	(heated,cON)      = N
	(purgeN,cON)      = N
	(purgeH,cON)      = N
	(purged,cON)      = N
	(catalyze,cON)    = wash
	(catalyzed,cON)   = N
	(wash,cON)        = N
	(washed,cON)      = catalyzed
	(raise,cON)       = N
	(raised,cON)      = N
	(vent,cON)        = N
	(vented,cON)      = N
	(transfer,cON)    = N
	(transferred,cON) = N
	(end,cON)         = N
	
	(start,cOFF)       = N
	(charge,cOFF)      = N
	(charged,cOFF)     = N
	(heat,cOFF)        = N
	(heated,cOFF)      = N
	(purgeN,cOFF)      = N
	(purgeH,cOFF)      = N
	(purged,cOFF)      = N
	(catalyze,cOFF)    = catalyze
	(catalyzed,cOFF)   = N
	(wash,cOFF)        = N
	(washed,cOFF)      = washed
	(raise,cOFF)       = N
	(raised,cOFF)      = N
	(vent,cOFF)        = N
	(vented,cOFF)      = N
	(transfer,cOFF)    = N
	(transferred,cOFF) = N
	(end,cOFF)         = N
	
	(start,cCLSD)       = N
	(charge,cCLSD)      = N
	(charged,cCLSD)     = N
	(heat,cCLSD)        = N
	(heated,cCLSD)      = N
	(purgeN,cCLSD)      = N
	(purgeH,cCLSD)      = N
	(purged,cCLSD)      = N
	(catalyze,cCLSD)    = wash
	(catalyzed,cCLSD)   = N
	(wash,cCLSD)        = N
	(washed,cCLSD)      = catalyzed
	(raise,cCLSD)       = N
	(raised,cCLSD)      = N
	(vent,cCLSD)        = N
	(vented,cCLSD)      = N
	(transfer,cCLSD)    = N
	(transferred,cCLSD) = N
	(end,cCLSD)         = N
	
end


begin CONCRETE_BEHAVIOUR where

	C => [ if  (c >= CATALYZE) -> 
					valveOpenC := TRUE;
					pumpOnC := TRUE;
					tankAmtC := tankAmtC - constAmtC
			|  (c >= WASHED) -> 
					pumpOnC := FALSE;
					valveOpenC := FALSE
		    | ~(c >= CATALYZE || c >= WASHED) -> 
					skip
		   fi ]		
	
end