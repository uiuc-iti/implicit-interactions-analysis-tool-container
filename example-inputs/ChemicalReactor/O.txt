begin AGENT where

	O := (oOPEN ; oON) + (oOFF ; oCLSD)

end


begin NEXT_BEHAVIOUR where
	
	(start,oOPEN)       = oOPEN
	(charge,oOPEN)      = oOPEN
	(charged,oOPEN)     = oOPEN
	(heat,oOPEN)        = oOPEN
	(heated,oOPEN)      = oOPEN
	(purgeN,oOPEN)      = oOPEN
	(purgeH,oOPEN)      = oOPEN
	(purged,oOPEN)      = oOPEN
	(catalyze,oOPEN)    = oOPEN
	(catalyzed,oOPEN)   = oOPEN
	(wash,oOPEN)        = oOPEN
	(washed,oOPEN)      = oOPEN
	(raise,oOPEN)       = oOPEN
	(raised,oOPEN)      = oOPEN
	(vent,oOPEN)        = oOPEN
	(vented,oOPEN)      = oOPEN
	(transfer,oOPEN)    = oOPEN
	(transferred,oOPEN) = oOPEN
	(end,oOPEN)         = oOFF
	
	(start,oON)       = oON
	(charge,oON)      = oON
	(charged,oON)     = oON
	(heat,oON)        = oON
	(heated,oON)      = oON
	(purgeN,oON)      = oON
	(purgeH,oON)      = oON
	(purged,oON)      = oON
	(catalyze,oON)    = oON
	(catalyzed,oON)   = oON
	(wash,oON)        = oON
	(washed,oON)      = oON
	(raise,oON)       = oON
	(raised,oON)      = oON
	(vent,oON)        = oON
	(vented,oON)      = oON
	(transfer,oON)    = oON
	(transferred,oON) = oON
	(end,oON)         = oCLSD
	
	(start,oOFF)       = oOFF
	(charge,oOFF)      = oOFF
	(charged,oOFF)     = oOFF
	(heat,oOFF)        = oOFF
	(heated,oOFF)      = oOFF
	(purgeN,oOFF)      = oOFF
	(purgeH,oOFF)      = oOFF
	(purged,oOFF)      = oOFF
	(catalyze,oOFF)    = oOFF
	(catalyzed,oOFF)   = oOFF
	(wash,oOFF)        = oOFF
	(washed,oOFF)      = oOFF
	(raise,oOFF)       = oOFF
	(raised,oOFF)      = oOFF
	(vent,oOFF)        = oOFF
	(vented,oOFF)      = oOFF
	(transfer,oOFF)    = oOPEN
	(transferred,oOFF) = oOFF
	(end,oOFF)         = oOFF
	
	(start,oCLSD)       = oCLSD
	(charge,oCLSD)      = oCLSD
	(charged,oCLSD)     = oCLSD
	(heat,oCLSD)        = oCLSD
	(heated,oCLSD)      = oCLSD
	(purgeN,oCLSD)      = oCLSD
	(purgeH,oCLSD)      = oCLSD
	(purged,oCLSD)      = oCLSD
	(catalyze,oCLSD)    = oCLSD
	(catalyzed,oCLSD)   = oCLSD
	(wash,oCLSD)        = oCLSD
	(washed,oCLSD)      = oCLSD
	(raise,oCLSD)       = oCLSD
	(raised,oCLSD)      = oCLSD
	(vent,oCLSD)        = oCLSD
	(vented,oCLSD)      = oCLSD
	(transfer,oCLSD)    = oON
	(transferred,oCLSD) = oCLSD
	(end,oCLSD)         = oCLSD
	

end


begin NEXT_STIMULUS where

	(start,oOPEN)       = N
	(charge,oOPEN)      = N
	(charged,oOPEN)     = N
	(heat,oOPEN)        = N
	(heated,oOPEN)      = N
	(purgeN,oOPEN)      = N
	(purgeH,oOPEN)      = N
	(purged,oOPEN)      = N
	(catalyze,oOPEN)    = N
	(catalyzed,oOPEN)   = N
	(wash,oOPEN)        = N
	(washed,oOPEN)      = N
	(raise,oOPEN)       = N
	(raised,oOPEN)      = N
	(vent,oOPEN)        = N
	(vented,oOPEN)      = N
	(transfer,oOPEN)    = transfer
	(transferred,oOPEN) = N
	(end,oOPEN)         = N
	
	(start,oON)       = N
	(charge,oON)      = N
	(charged,oON)     = N
	(heat,oON)        = N
	(heated,oON)      = N
	(purgeN,oON)      = N
	(purgeH,oON)      = N
	(purged,oON)      = N
	(catalyze,oON)    = N
	(catalyzed,oON)   = N
	(wash,oON)        = N
	(washed,oON)      = N
	(raise,oON)       = N
	(raised,oON)      = N
	(vent,oON)        = N
	(vented,oON)      = N
	(transfer,oON)    = transferred
	(transferred,oON) = N
	(end,oON)         = N
	
	(start,oOFF)       = N
	(charge,oOFF)      = N
	(charged,oOFF)     = N
	(heat,oOFF)        = N
	(heated,oOFF)      = N
	(purgeN,oOFF)      = N
	(purgeH,oOFF)      = N
	(purged,oOFF)      = N
	(catalyze,oOFF)    = N
	(catalyzed,oOFF)   = N
	(wash,oOFF)        = N
	(washed,oOFF)      = N
	(raise,oOFF)       = N
	(raised,oOFF)      = N
	(vent,oOFF)        = N
	(vented,oOFF)      = N
	(transfer,oOFF)    = transfer
	(transferred,oOFF) = N
	(end,oOFF)         = N
	
	(start,oCLSD)       = N
	(charge,oCLSD)      = N
	(charged,oCLSD)     = N
	(heat,oCLSD)        = N
	(heated,oCLSD)      = N
	(purgeN,oCLSD)      = N
	(purgeH,oCLSD)      = N
	(purged,oCLSD)      = N
	(catalyze,oCLSD)    = N
	(catalyzed,oCLSD)   = N
	(wash,oCLSD)        = N
	(washed,oCLSD)      = N
	(raise,oCLSD)       = N
	(raised,oCLSD)      = N
	(vent,oCLSD)        = N
	(vented,oCLSD)      = N
	(transfer,oCLSD)    = transferred
	(transferred,oCLSD) = N
	(end,oCLSD)         = N
	
end


begin CONCRETE_BEHAVIOUR where

	O => [ if  (c >= TRANSFER) -> 
					valveOpenO := TRUE;
					pumpOnO := TRUE;
					tankAmtO := reactorAmt
			|  (c >= END) -> 
					pumpOnO := FALSE;
					valveOpenO := FALSE
		    | ~(c >= TRANSFER || c >= END) -> 
					skip
		   fi ]		
	
end