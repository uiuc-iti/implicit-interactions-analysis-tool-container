begin AGENT where

	R := rIDLE + rINIT + rHEAT1 + rHEAT2 + rPRGE1 + rPRGE2 + rPRGE3 + rPRES1 + rPRES2 + rVENT + rTRNS

end


begin NEXT_BEHAVIOUR where
	
	(start,rIDLE)       = rINIT
	(charge,rIDLE)      = rIDLE
	(charged,rIDLE)     = rIDLE
	(heat,rIDLE)        = rIDLE
	(heated,rIDLE)      = rIDLE
	(purgeN,rIDLE)      = rIDLE
	(purgeH,rIDLE)      = rIDLE
	(purged,rIDLE)      = rIDLE
	(catalyze,rIDLE)    = rIDLE
	(catalyzed,rIDLE)   = rPRGE3
	(wash,rIDLE)        = rIDLE
	(washed,rIDLE)      = rIDLE
	(raise,rIDLE)       = rIDLE
	(raised,rIDLE)      = rIDLE
	(vent,rIDLE)        = rIDLE
	(vented,rIDLE)      = rIDLE
	(transfer,rIDLE)    = rIDLE
	(transferred,rIDLE) = rIDLE
	(end,rIDLE)         = rIDLE
	
	(start,rINIT)       = rINIT
	(charge,rINIT)      = rINIT
	(charged,rINIT)     = rHEAT1
	(heat,rINIT)        = rINIT
	(heated,rINIT)      = rINIT
	(purgeN,rINIT)      = rINIT
	(purgeH,rINIT)      = rINIT
	(purged,rINIT)      = rINIT
	(catalyze,rINIT)    = rINIT
	(catalyzed,rINIT)   = rINIT
	(wash,rINIT)        = rINIT
	(washed,rINIT)      = rINIT
	(raise,rINIT)       = rINIT
	(raised,rINIT)      = rINIT
	(vent,rINIT)        = rINIT
	(vented,rINIT)      = rINIT
	(transfer,rINIT)    = rINIT
	(transferred,rINIT) = rINIT
	(end,rINIT)         = rINIT
	
	(start,rHEAT1)       = rHEAT1
	(charge,rHEAT1)      = rHEAT1
	(charged,rHEAT1)     = rHEAT1
	(heat,rHEAT1)        = rHEAT1
	(heated,rHEAT1)      = rPRGE1
	(purgeN,rHEAT1)      = rHEAT1
	(purgeH,rHEAT1)      = rHEAT1
	(purged,rHEAT1)      = rHEAT1
	(catalyze,rHEAT1)    = rHEAT1
	(catalyzed,rHEAT1)   = rHEAT1
	(wash,rHEAT1)        = rHEAT1
	(washed,rHEAT1)      = rHEAT1
	(raise,rHEAT1)       = rHEAT1
	(raised,rHEAT1)      = rHEAT1
	(vent,rHEAT1)        = rHEAT1
	(vented,rHEAT1)      = rHEAT1
	(transfer,rHEAT1)    = rHEAT1
	(transferred,rHEAT1) = rHEAT1
	(end,rHEAT1)         = rHEAT1
	
	(start,rHEAT2)       = rHEAT2
	(charge,rHEAT2)      = rHEAT2
	(charged,rHEAT2)     = rHEAT2
	(heat,rHEAT2)        = rHEAT2
	(heated,rHEAT2)      = rTRNS
	(purgeN,rHEAT2)      = rHEAT2
	(purgeH,rHEAT2)      = rHEAT2
	(purged,rHEAT2)      = rHEAT2
	(catalyze,rHEAT2)    = rHEAT2
	(catalyzed,rHEAT2)   = rHEAT2
	(wash,rHEAT2)        = rHEAT2
	(washed,rHEAT2)      = rHEAT2
	(raise,rHEAT2)       = rHEAT2
	(raised,rHEAT2)      = rHEAT2
	(vent,rHEAT2)        = rHEAT2
	(vented,rHEAT2)      = rHEAT2
	(transfer,rHEAT2)    = rHEAT2
	(transferred,rHEAT2) = rHEAT2
	(end,rHEAT2)         = rHEAT2
	
	(start,rPRGE1)       = rPRGE1
	(charge,rPRGE1)      = rPRGE1
	(charged,rPRGE1)     = rPRGE1
	(heat,rPRGE1)        = rPRGE1
	(heated,rPRGE1)      = rPRGE1
	(purgeN,rPRGE1)      = rPRGE1
	(purgeH,rPRGE1)      = rPRGE1
	(purged,rPRGE1)      = rIDLE
	(catalyze,rPRGE1)    = rPRGE1
	(catalyzed,rPRGE1)   = rPRGE1
	(wash,rPRGE1)        = rPRGE1
	(washed,rPRGE1)      = rPRGE1
	(raise,rPRGE1)       = rPRGE1
	(raised,rPRGE1)      = rPRGE1
	(vent,rPRGE1)        = rPRGE1
	(vented,rPRGE1)      = rPRGE1
	(transfer,rPRGE1)    = rPRGE1
	(transferred,rPRGE1) = rPRGE1
	(end,rPRGE1)         = rPRGE1

	(start,rPRGE2)       = rPRGE2
	(charge,rPRGE2)      = rPRGE2
	(charged,rPRGE2)     = rPRGE2
	(heat,rPRGE2)        = rPRGE2
	(heated,rPRGE2)      = rPRGE2
	(purgeN,rPRGE2)      = rPRGE2
	(purgeH,rPRGE2)      = rPRGE2
	(purged,rPRGE2)      = rHEAT2
	(catalyze,rPRGE2)    = rPRGE2
	(catalyzed,rPRGE2)   = rPRGE2
	(wash,rPRGE2)        = rPRGE2
	(washed,rPRGE2)      = rPRGE2
	(raise,rPRGE2)       = rPRGE2
	(raised,rPRGE2)      = rPRGE2
	(vent,rPRGE2)        = rPRGE2
	(vented,rPRGE2)      = rPRGE2
	(transfer,rPRGE2)    = rPRGE2
	(transferred,rPRGE2) = rPRGE2
	(end,rPRGE2)         = rPRGE2
	
	(start,rPRGE3)       = rPRGE3
	(charge,rPRGE3)      = rPRGE3
	(charged,rPRGE3)     = rPRGE3
	(heat,rPRGE3)        = rPRGE3
	(heated,rPRGE3)      = rPRGE3
	(purgeN,rPRGE3)      = rPRGE3
	(purgeH,rPRGE3)      = rPRGE3
	(purged,rPRGE3)      = rPRES1
	(catalyze,rPRGE3)    = rPRGE3
	(catalyzed,rPRGE3)   = rPRGE3
	(wash,rPRGE3)        = rPRGE3
	(washed,rPRGE3)      = rPRGE3
	(raise,rPRGE3)       = rPRGE3
	(raised,rPRGE3)      = rPRGE3
	(vent,rPRGE3)        = rPRGE3
	(vented,rPRGE3)      = rPRGE3
	(transfer,rPRGE3)    = rPRGE3
	(transferred,rPRGE3) = rPRGE3
	(end,rPRGE3)         = rPRGE3
	
	(start,rPRES1)       = rPRES1
	(charge,rPRES1)      = rPRES1
	(charged,rPRES1)     = rPRES1
	(heat,rPRES1)        = rPRES1
	(heated,rPRES1)      = rPRES1
	(purgeN,rPRES1)      = rPRES1
	(purgeH,rPRES1)      = rPRES1
	(purged,rPRES1)      = rPRES1
	(catalyze,rPRES1)    = rPRES1
	(catalyzed,rPRES1)   = rPRES1
	(wash,rPRES1)        = rPRES1
	(washed,rPRES1)      = rPRES1
	(raise,rPRES1)       = rPRES1
	(raised,rPRES1)      = rPRES2
	(vent,rPRES1)        = rPRES1
	(vented,rPRES1)      = rPRES1
	(transfer,rPRES1)    = rPRES1
	(transferred,rPRES1) = rPRES1
	(end,rPRES1)         = rPRES1
	
	(start,rPRES2)       = rPRES2
	(charge,rPRES2)      = rPRES2
	(charged,rPRES2)     = rPRES2
	(heat,rPRES2)        = rPRES2
	(heated,rPRES2)      = rPRES2
	(purgeN,rPRES2)      = rPRES2
	(purgeH,rPRES2)      = rPRES2
	(purged,rPRES2)      = rPRES2
	(catalyze,rPRES2)    = rPRES2
	(catalyzed,rPRES2)   = rPRES2
	(wash,rPRES2)        = rPRES2
	(washed,rPRES2)      = rPRES2
	(raise,rPRES2)       = rPRES2
	(raised,rPRES2)      = rVENT
	(vent,rPRES2)        = rPRES2
	(vented,rPRES2)      = rPRES2
	(transfer,rPRES2)    = rPRES2
	(transferred,rPRES2) = rPRES2
	(end,rPRES2)         = rPRES2
	
	(start,rVENT)       = rVENT
	(charge,rVENT)      = rVENT
	(charged,rVENT)     = rVENT
	(heat,rVENT)        = rVENT
	(heated,rVENT)      = rVENT
	(purgeN,rVENT)      = rVENT
	(purgeH,rVENT)      = rVENT
	(purged,rVENT)      = rVENT
	(catalyze,rVENT)    = rVENT
	(catalyzed,rVENT)   = rVENT
	(wash,rVENT)        = rVENT
	(washed,rVENT)      = rVENT
	(raise,rVENT)       = rVENT
	(raised,rVENT)      = rVENT
	(vent,rVENT)        = rVENT
	(vented,rVENT)      = rPRGE2
	(transfer,rVENT)    = rVENT
	(transferred,rVENT) = rVENT
	(end,rVENT)         = rVENT
	
	(start,rTRNS)       = rTRNS
	(charge,rTRNS)      = rTRNS
	(charged,rTRNS)     = rTRNS
	(heat,rTRNS)        = rTRNS
	(heated,rTRNS)      = rTRNS
	(purgeN,rTRNS)      = rTRNS
	(purgeH,rTRNS)      = rTRNS
	(purged,rTRNS)      = rTRNS
	(catalyze,rTRNS)    = rTRNS
	(catalyzed,rTRNS)   = rTRNS
	(wash,rTRNS)        = rTRNS
	(washed,rTRNS)      = rTRNS
	(raise,rTRNS)       = rTRNS
	(raised,rTRNS)      = rTRNS
	(vent,rTRNS)        = rTRNS
	(vented,rTRNS)      = rTRNS
	(transfer,rTRNS)    = rTRNS
	(transferred,rTRNS) = rIDLE
	(end,rTRNS)         = rTRNS

end


begin NEXT_STIMULUS where

	(start,rIDLE)       = N
	(charge,rIDLE)      = N
	(charged,rIDLE)     = N
	(heat,rIDLE)        = N
	(heated,rIDLE)      = N
	(purgeN,rIDLE)      = N
	(purgeH,rIDLE)      = N
	(purged,rIDLE)      = N
	(catalyze,rIDLE)    = N
	(catalyzed,rIDLE)   = purgeH
	(wash,rIDLE)        = N
	(washed,rIDLE)      = N
	(raise,rIDLE)       = N
	(raised,rIDLE)      = N
	(vent,rIDLE)        = N
	(vented,rIDLE)      = N
	(transfer,rIDLE)    = N
	(transferred,rIDLE) = N
	(end,rIDLE)         = N
	
	(start,rINIT)       = N
	(charge,rINIT)      = N
	(charged,rINIT)     = heat
	(heat,rINIT)        = N
	(heated,rINIT)      = N
	(purgeN,rINIT)      = N
	(purgeH,rINIT)      = N
	(purged,rINIT)      = N
	(catalyze,rINIT)    = N
	(catalyzed,rINIT)   = N
	(wash,rINIT)        = N
	(washed,rINIT)      = N
	(raise,rINIT)       = N
	(raised,rINIT)      = N
	(vent,rINIT)        = N
	(vented,rINIT)      = N
	(transfer,rINIT)    = N
	(transferred,rINIT) = N
	(end,rINIT)         = N
	
	(start,rHEAT1)       = N
	(charge,rHEAT1)      = N
	(charged,rHEAT1)     = N
	(heat,rHEAT1)        = N
	(heated,rHEAT1)      = purgeN
	(purgeN,rHEAT1)      = N
	(purgeH,rHEAT1)      = N
	(purged,rHEAT1)      = N
	(catalyze,rHEAT1)    = N
	(catalyzed,rHEAT1)   = N
	(wash,rHEAT1)        = N
	(washed,rHEAT1)      = N
	(raise,rHEAT1)       = N
	(raised,rHEAT1)      = N
	(vent,rHEAT1)        = N
	(vented,rHEAT1)      = N
	(transfer,rHEAT1)    = N
	(transferred,rHEAT1) = N
	(end,rHEAT1)         = N
	
	(start,rHEAT2)       = N
	(charge,rHEAT2)      = N
	(charged,rHEAT2)     = N
	(heat,rHEAT2)        = N
	(heated,rHEAT2)      = N
	(purgeN,rHEAT2)      = N
	(purgeH,rHEAT2)      = N
	(purged,rHEAT2)      = N
	(catalyze,rHEAT2)    = N
	(catalyzed,rHEAT2)   = N
	(wash,rHEAT2)        = N
	(washed,rHEAT2)      = N
	(raise,rHEAT2)       = N
	(raised,rHEAT2)      = N
	(vent,rHEAT2)        = N
	(vented,rHEAT2)      = N
	(transfer,rHEAT2)    = N
	(transferred,rHEAT2) = N
	(end,rHEAT2)         = N
	
	(start,rPRGE1)       = N
	(charge,rPRGE1)      = N
	(charged,rPRGE1)     = N
	(heat,rPRGE1)        = N
	(heated,rPRGE1)      = N
	(purgeN,rPRGE1)      = N
	(purgeH,rPRGE1)      = N
	(purged,rPRGE1)      = N
	(catalyze,rPRGE1)    = N
	(catalyzed,rPRGE1)   = N
	(wash,rPRGE1)        = N
	(washed,rPRGE1)      = N
	(raise,rPRGE1)       = N
	(raised,rPRGE1)      = N
	(vent,rPRGE1)        = N
	(vented,rPRGE1)      = N
	(transfer,rPRGE1)    = N
	(transferred,rPRGE1) = N
	(end,rPRGE1)         = N
	
	(start,rPRGE2)       = N
	(charge,rPRGE2)      = N
	(charged,rPRGE2)     = N
	(heat,rPRGE2)        = N
	(heated,rPRGE2)      = N
	(purgeN,rPRGE2)      = N
	(purgeH,rPRGE2)      = N
	(purged,rPRGE2)      = heat
	(catalyze,rPRGE2)    = N
	(catalyzed,rPRGE2)   = N
	(wash,rPRGE2)        = N
	(washed,rPRGE2)      = N
	(raise,rPRGE2)       = N
	(raised,rPRGE2)      = N
	(vent,rPRGE2)        = N
	(vented,rPRGE2)      = N
	(transfer,rPRGE2)    = N
	(transferred,rPRGE2) = N
	(end,rPRGE2)         = N
	
	(start,rPRGE3)       = N
	(charge,rPRGE3)      = N
	(charged,rPRGE3)     = N
	(heat,rPRGE3)        = N
	(heated,rPRGE3)      = N
	(purgeN,rPRGE3)      = N
	(purgeH,rPRGE3)      = N
	(purged,rPRGE3)      = raise
	(catalyze,rPRGE3)    = N
	(catalyzed,rPRGE3)   = N
	(wash,rPRGE3)        = N
	(washed,rPRGE3)      = N
	(raise,rPRGE3)       = N
	(raised,rPRGE3)      = N
	(vent,rPRGE3)        = N
	(vented,rPRGE3)      = N
	(transfer,rPRGE3)    = N
	(transferred,rPRGE3) = N
	(end,rPRGE3)         = N
	
	(start,rPRES1)       = N
	(charge,rPRES1)      = N
	(charged,rPRES1)     = N
	(heat,rPRES1)        = N
	(heated,rPRES1)      = N
	(purgeN,rPRES1)      = N
	(purgeH,rPRES1)      = N
	(purged,rPRES1)      = N
	(catalyze,rPRES1)    = N
	(catalyzed,rPRES1)   = N
	(wash,rPRES1)        = N
	(washed,rPRES1)      = N
	(raise,rPRES1)       = N
	(raised,rPRES1)      = raise
	(vent,rPRES1)        = N
	(vented,rPRES1)      = N
	(transfer,rPRES1)    = N
	(transferred,rPRES1) = N
	(end,rPRES1)         = N
	
	(start,rPRES2)       = N
	(charge,rPRES2)      = N
	(charged,rPRES2)     = N
	(heat,rPRES2)        = N
	(heated,rPRES2)      = N
	(purgeN,rPRES2)      = N
	(purgeH,rPRES2)      = N
	(purged,rPRES2)      = N
	(catalyze,rPRES2)    = N
	(catalyzed,rPRES2)   = N
	(wash,rPRES2)        = N
	(washed,rPRES2)      = N
	(raise,rPRES2)       = N
	(raised,rPRES2)      = vent
	(vent,rPRES2)        = N
	(vented,rPRES2)      = N
	(transfer,rPRES2)    = N
	(transferred,rPRES2) = N
	(end,rPRES2)         = N
	
	(start,rVENT)       = N
	(charge,rVENT)      = N
	(charged,rVENT)     = N
	(heat,rVENT)        = N
	(heated,rVENT)      = N
	(purgeN,rVENT)      = N
	(purgeH,rVENT)      = N
	(purged,rVENT)      = N
	(catalyze,rVENT)    = N
	(catalyzed,rVENT)   = N
	(wash,rVENT)        = N
	(washed,rVENT)      = N
	(raise,rVENT)       = N
	(raised,rVENT)      = N
	(vent,rVENT)        = N
	(vented,rVENT)      = purgeN
	(transfer,rVENT)    = N
	(transferred,rVENT) = N
	(end,rVENT)         = N
	
	(start,rTRNS)       = N
	(charge,rTRNS)      = N
	(charged,rTRNS)     = N
	(heat,rTRNS)        = N
	(heated,rTRNS)      = N
	(purgeN,rTRNS)      = N
	(purgeH,rTRNS)      = N
	(purged,rTRNS)      = N
	(catalyze,rTRNS)    = N
	(catalyzed,rTRNS)   = N
	(wash,rTRNS)        = N
	(washed,rTRNS)      = N
	(raise,rTRNS)       = N
	(raised,rTRNS)      = N
	(vent,rTRNS)        = N
	(vented,rTRNS)      = N
	(transfer,rTRNS)    = N
	(transferred,rTRNS) = end
	(end,rTRNS)         = N
				
end


begin CONCRETE_BEHAVIOUR where

	R => [ if  (c >= START && reactorState = IDLE) -> 
					reactorAmt := 0;
					temperatureR := 0;
					pressureR := 0;
					reactorState := INIT
			|  (c >= CHARGED && reactorState = INIT) -> 
					reactorAmt := reactorAmt + constAmtA + constAmtB;
					temperatureSetP := 55;
					reactorState := HEATING1
			|  (c >= HEATED && reactorState = HEATING1) -> 
					pressureSetP := 10;
					reactorState := PURGING1
			|  (c >= HEATED && reactorState = HEATING2) -> 
					pressureSetP := 5;
					reactorState := TRANSFERRING
			|  (c >= PURGED && reactorState = PURGING1) -> 
					reactorState := IDLE			
			|  (c >= PURGED && reactorState = PURGING2) -> 
					temperatureSetP := 70;
					reactorState := HEATING2
			|  (c >= CATALYZED && reactorState = IDLE) -> 
					reactorAmt := reactorAmt + constAmtC + constWashAmtA + constWashAmtB;
					pressureSetP := 10;
					reactorState := PURGING3								
			|  (c >= PURGED && reactorState = PURGING3) -> 
					pressureSetP := 10;
					holdSetP := 150;
					reactorState := PRESSURIZING1
			|  (c >= RAISED && reactorState = PRESSURIZING1) -> 
					pressureSetP := 20;
					holdSetP := 20;
					reactorState := PRESSURIZING2					
			|  (c >= RAISED && reactorState = PRESSURIZING2) -> 
					pressureSetP := 1;
					reactorState := VENTING				
			|  (c >= VENTED && reactorState = VENTING) -> 
					pressureSetP := 15;
					reactorState := PURGING2
			|  (c >= TRANSFERRED && reactorState = TRANSFERRING) -> 
					reactorState := IDLE	
		    | ~((c >= START && reactorState = IDLE) 			||
				(c >= CHARGED && reactorState = INIT) 			||
				(c >= HEATED && reactorState = HEATING1) 		||
				(c >= HEATED && reactorState = HEATING2)		|| 
				(c >= PURGED && reactorState = PURGING1)		||
				(c >= PURGED && reactorState = PURGING2)		||
				(c >= CATALYZED && reactorState = IDLE)			||
				(c >= PURGED && reactorState = PURGING3)		||
				(c >= RAISED && reactorState = PRESSURIZING1)	||
				(c >= RAISED && reactorState = PRESSURIZING2)	||
				(c >= VENTED && reactorState = VENTING) 		||
				(c >= TRANSFERRED && reactorState = TRANSFERRING)) -> 
					skip
		   fi ]		
	
end