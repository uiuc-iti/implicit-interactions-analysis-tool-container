"""
sets a given specification folder as the target of the ImplicitInteractionsAnalysisTool and runs the tool

run as 'python3 run.py <specification>'
where <specification> is a c2ka specification folder in the 'spec' folder

run AFTER 'python3 generate_lhs.py <specification>' if 'src' folder has no .lhs for the specification yet

"""
import sys, os, glob


def read_lines(filename):
  with open(filename, "r") as f:
    return  f.read().split(os.linesep)
    

def write_lines(filename, lines, sep=""):
  with open(filename, "w") as f:
    for line in lines:
      f.write(line + sep)


def get_valid_choices():
  return os.listdir("specs/")


def print_help():
  spec_folder_names = get_valid_choices()
  print("Possible system choices are:")
  for name in spec_folder_names:
    print("\t" + name)
  
 
def create_dir_if_not_exists(directory_path):
  if not os.path.exists(directory_path):
    os.makedirs(directory_path)
  
  
def run_C2KA_Tool():
  # we expect this to be the name of the haskell defining the system AND 
  #the name of the folder containing spec files for each agent
  project_name = sys.argv[1] 

  # 1. get the name of the function within the haskell module named project_name
  project_module_content = read_lines("src/" + project_name + ".lhs")
  for line in project_module_content:
    if line.startswith("module " + project_name):
      # here we assume the line follows the pattern: 
      #      module <project_name> ( <function_name> ) where
      function_name = line.split()[3]  
    elif line.strip().startswith("let soca ="):
      # here we assume the line follows the pattern
      #	let soca = newSoCA "<maude spec name>" stimSet ckaSet constants
      maude_spec_name = line.split()[4].strip("\"")

  print(function_name, maude_spec_name)

  # 2. write configuration in MaudeInterface.lhs
  maude_interface_content = read_lines("src/MaudeInterface.lhs")
  for i in range(len(maude_interface_content)):
    if maude_interface_content[i].startswith("maudeRewrite term = do"):
      maude_interface_content[i+1] = "\trewrite <- runMaude conf $ Rewrite term"
      maude_interface_content[i+2] = ""
    elif maude_interface_content[i].startswith("-- FOR LINUX USAGE"):
      maude_interface_content[i+1] = "conf :: MaudeConf"
      maude_interface_content[i+2] = "conf = MaudeConf { maudeCmd  = \"maude/maude-core/linux/maude.linux64\" , loadFiles = [\"maude/" + maude_spec_name.lower() + "/" + maude_spec_name + ".maude\"]  }"
      maude_interface_content[i+3] = ""
      break
  write_lines("src/MaudeInterface.lhs", maude_interface_content, sep=os.linesep)

  # 3. ensure project is imported in Test.lhs and set main
  test_content = read_lines("src/Test.lhs")
  import_indices = [n for n,line in enumerate(test_content) if line.startswith("import")]
  if not any([test_content[i] == "import " + project_name for i in import_indices]):
    test_content.insert(import_indices[0], "import " + project_name)
  for i in range(len(test_content)):
    if test_content[i] == "main :: IO ()":
      test_content[i+1] = "main = " + function_name
      break
  write_lines("src/Test.lhs", test_content, sep=os.linesep)

  # 4. ensure all .lhs files are included in makefile
  makefile_contents = read_lines("Makefile")
  for i in range(len(makefile_contents)):
    if makefile_contents[i].startswith("LHS_SRC ="):
      start = i
    elif makefile_contents[i].startswith("SRC_DIR ="):
      end = i
      break
  makefile_line = "\t" + project_name + ".lhs\t\\"
  if makefile_line not in makefile_contents[start:end]:
    makefile_contents.insert(start + 1, makefile_line)
    write_lines("Makefile", makefile_contents, sep=os.linesep)

  # 5. create a folder for the maude spec if one does not exist
  create_dir_if_not_exists("maude/" + maude_spec_name.lower())

  # 6. build and run C2KA-Tool
  os.system("make")
  print("project compiled")
  results_folder = "results"
  create_dir_if_not_exists(results_folder)
  print("starting c2ka-tool")
  os.system("./ImplicitInteractionsAnalysisTool > " + results_folder + "/" + project_name + ".txt")
  
  
if len(sys.argv) < 2:
  print("no system name specified!")
elif len(sys.argv) > 2:
  print("too many arguements!")
elif any([sys.argv[1] == cmd for cmd in ["-h", "-help"]]):
  print_help()
elif sys.argv[1] in get_valid_choices():
  run_C2KA_Tool()
else:
  print("invalid system name, use -h to see valid choices")
