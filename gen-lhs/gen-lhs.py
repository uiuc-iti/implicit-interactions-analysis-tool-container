"""

run as 'python3 gen-lhs.py <specification> <projectname>'

where <specification> is a c2ka specification file (e.g., project.json)

<projectname> is the name that should be used for this specification


the json file should contain the stim_set, cka_set, intended interaction tree, and root(s) of that tree (see lines 44-48)



"""



import sys, os, glob, json





def read(filename):

  with open(filename, "r") as f:

    return  f.read()





def write(filename, content):

  with open(filename, "w") as f:

    f.write(content)





def list_to_string(l):

  results = "["

  for item in l:

    results += str(item) + "," if isinstance(item, int) else "\"" + item + "\", "

  return results[:-2] + "]"

  



def edge_lists_to_strings(edges):

  result = []

 

  for edge in edges:

    result.append("((" + str(edge[0][0]) + ", \"" + str(edge[0][1]) + "\"),(" + str(edge[1][0]) + ", \"" + str(edge[1][1]) + "\")," + str(edge[2]) + ")")

   

  return result





def run_lhs_generator():

  config_json_file = sys.argv[1]
  project_name = sys.argv[2] 

  camel_case_project_name = project_name[0].lower() + project_name[1:]

  configuration = json.loads(read(config_json_file))

  

  edges = edge_lists_to_strings(configuration["edges"])

  stim_set= list_to_string(configuration["stim_set"])

  cka_set = list_to_string(configuration["cka_set"])

  constants = list_to_string(configuration["constants"])

  starting_agents = configuration["starting_agent"]

  

  code = "{code}"

  maude_system_name = project_name.upper()

  

  file_contents = f"""\\begin{code}

module {project_name} ( {camel_case_project_name}Simulation ) where

import Agent          ( AgentName, load, expr, spec )

import Analysis       ( runAnalysis )

import CommPaths      ( CommType (..) ,Paths, printPaths )						

import Intended       ( getAgents, mkIntTree, mkMatrix, intPaths )

import MaudeInterface ( generateMaudeSpec, generateMaudeSOCA )

import SoCA           ( SoCA, addAgent, newSoCA, printSoCA, setStimSet, setCKASet, setConstants )



-- For testing Influencing Stimuli

import InfluencingStimuli

import SoCAPrinter	



-- For SIMULATION

import Simulation

import Data.Text as Text ( Text, concat, pack, unpack )

import Data.Set ( member, toList )

import Data.List ( elemIndices, take )

import Behaviour

import Stimulus	

import Debug.Trace          



{camel_case_project_name}Simulation :: IO ()

{camel_case_project_name}Simulation = do

	-- ESTABLISH SETS OF BASIC STIMULI AND BEHAVIORS

	let stimSet = setStimSet {stim_set} 

	let ckaSet = setCKASet {cka_set}

	let constants = setConstants {constants}

	

	-- CREATE A NEW SYSTEM OF COMMUNICATION AGENTS

	let soca = newSoCA \"{maude_system_name}\" stimSet ckaSet constants

	

	-- LOAD ALL OF THE AGENT SPECIFICATIONS"""

	

  file_contents += os.linesep

  agent_filenames = {x.split("/")[-1]: x for x in glob.glob(os.path.dirname(config_json_file) + "/*.txt")}

  for agent_filename, agent_path in agent_filenames.items():

    file_contents += "\tagent" + agent_filename.split(".")[0].upper() + " <- load \"specs/input/" + agent_filename + "\"" + os.linesep


  file_contents += os.linesep + "\t-- ADD ALL OF THE AGENTS TO THE SYSTEM" + os.linesep

  for agent_filename in agent_filenames.keys():

    file_contents += "\tsoca <- addAgent agent" + agent_filename.split(".")[0].upper() + " soca" + os.linesep



  

  file_contents += """\tprintSoCA soca

	

	-- GENERATE THE MAUDE SPECIFICATIONS FOR EACH AGENT AND THE SYSTEM"""

	

  file_contents += os.linesep

  for agent_filename in agent_filenames.keys():

    file_contents += "\tgenerateMaudeSpec soca agent" + agent_filename.split(".")[0].upper() + os.linesep

    

	

  file_contents += """\tgenerateMaudeSOCA soca



	-- COMPUTE SET OF INTENDED INTERACTIONS

	let adjMatrix = mkMatrix edges

	let agents    = getAgents edges

	let intended  = """

	

  for starting_agent in starting_agents:

    starting_agent = list_to_string(starting_agent)

    starting_agent = starting_agent.replace("[", "(")

    starting_agent = starting_agent.replace("]", ")")

    file_contents += f"""(intPaths $ mkIntTree agents adjMatrix ({starting_agent},X))++"""

  file_contents = file_contents[:-2]

  file_contents += os.linesep

  file_contents += """\t-- RUN ANALYSIS TO IDENTIFY IMPLICIT INTERACTIONS AND COMPUTE SEVERITY

	ints <- runAnalysis soca intended



	"""

	

  if "-e" in sys.argv:

    file_contents += """-- RUN ANALYSIS OF EXPLOITABILITY OF IMPLICIT INTERACTIONS

	runExploit soca ints"""

  file_contents += """

	putStrLn \"\"



edges :: [((Int,AgentName),(Int,AgentName),CommType)]

edges =

	["""

	

  file_contents += os.linesep

  for edge in edges:

    file_contents += "\t\t" + edge + "," + os.linesep

  file_contents = file_contents[:-2]

  file_contents += os.linesep + """\t]	



runExploit :: SoCA -> Paths -> IO ()

runExploit s []     = putStr \"\"

runExploit s (p:ps) = do

	putStr   $ \"\\nImplicit Interaction = \"

	printPaths [p]

	putStr $ \"Attack = \"

	printSet $ attack s p	

	putStrLn $ \"Exploitability       = \" ++ show (exploit s p)

	runExploit s ps

\end{code}"""

  #write("src/" + project_name+".lhs", file_contents)

  print(file_contents)

  

  

if len(sys.argv) < 3:

  print("Must provide configuration and project name.")

else:

  run_lhs_generator()







