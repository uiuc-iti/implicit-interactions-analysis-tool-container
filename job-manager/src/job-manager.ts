import { ChildProcess, exec } from 'child_process';
import fs from 'fs';
import pathUtils from 'path';
import { exit } from 'process';

// ps -p 2337 -o %cpu,%mem,cmd | sed -n 2p | awk '{print "{"$1","$2"}"}'
// Cmd for launching { time ./ImplicitInteractionsAnalysisTool > OUTPUT.txt 2> ERROR.txt ; } 2> TIME.txt &

const JOB_FILE = "job.json";
const CANCEL_FILENAME = "CANCEL";
const DELETE_FILENAME = "DELETE";

require("dotenv").config();

const jobQ : string[] = [];
const runningProcesses = new Map<string, ChildProcess>();
const finishedJobs : string[] = [];
const workingDir = process.env.IIAT_WORKING_DIR;

if(!workingDir) {
    console.error("Working directory not found! (" + workingDir + ")");
    exit();
}

function searchForNewJobs() {
    if(workingDir && fs.existsSync(workingDir)) {
        fs.readdirSync(workingDir, { withFileTypes: true })
            .filter(dirent => dirent.isDirectory())
            .map(dirent => {
                let name = dirent.name;
                if(!finishedJobs.includes(name) && !runningProcesses.has(name) && !jobQ.includes(name)) {
                    try{
                        if(checkJobStatus(pathUtils.resolve(workingDir, name, JOB_FILE), "QUEUED")) {
                            console.log("Added new job to the queue: " + name);
                            jobQ.push(name);
                        }
                    } catch(err) {
                        console.error("Error in reading new job directory (name = "+ name +"). " + err);
                    }
                }
            })
    } else {
        console.error("Unable to find working directory! (workingDir = " + workingDir + ")");
    }
}



/**
 * Checks the jobfile's status field
 * @param jobFilePath - The path to the job file
 * @param expectedStatus - The expected status value
 * @returns Returns true if the status in the file matches the expected value.
 */
function checkJobStatus(jobFilePath: string, expectedStatus: string) : boolean {
    const statusData = fs.readFileSync(jobFilePath, {encoding: "utf-8"});
    const jobModel = JSON.parse(statusData);
    return expectedStatus === jobModel._status;
}

/**
 * Updates the jobfile's status field.
 * @param jobFilePath - The path to the job file that should be updated.
 * @param newStatus - The new status value.
 * @param onlyIf - The required old status value. The value will not be updated unless the current status matches this one.
 * @returns {boolean} Returns true if the status value was updated.
 */
function changeJobStatus(jobFilePath: string, newStatus: string, onlyIf? : string) : boolean {
    const statusData = fs.readFileSync(jobFilePath, {encoding: "utf-8"});
    const jobModel = JSON.parse(statusData);
    if(onlyIf && jobModel._status !== onlyIf) 
        return false;
    
    jobModel._status = newStatus
    fs.writeFileSync(jobFilePath, JSON.stringify(jobModel));
    return true;
}

function startJobs() {
    const numCpus = require('os').cpus().length;
    const numNewJobs = numCpus - runningProcesses.size;

    for(let i = 0; i < numNewJobs && jobQ.length > 0; i++) {
        const newJob = jobQ.shift();
        if(newJob && workingDir) {
            const newJobWorkingDir = pathUtils.resolve(workingDir, newJob);
            const statusFilePath = pathUtils.resolve(newJobWorkingDir, JOB_FILE);
            changeJobStatus(statusFilePath, "RUNNING");

            console.log("Starting job " + newJob);
            const cmd = `/usr/local/iiat/buildAndRun.sh ${newJobWorkingDir}`
            console.log("Executing: " + cmd);
            const proc = exec(cmd, (error, stdout, stderr) => {
                if(error) {
                    console.log(`Exec error (err=${error.code}): ${error.message}`);
                    return;
                }
                if(stderr) {
                    console.log(`Exec stderr: ${stderr}`);
                    return;
                }
                console.log(`Exec stdout: ${stdout}`);
            });
            runningProcesses.set(newJob, proc);
            
        }
    }

}

function finishRunningJobs() {
    if(workingDir) {
        runningProcesses.forEach((value: ChildProcess, key: string) => {
            const retCode = value.exitCode;
            if(retCode !== null) { // The process has exited
                console.log(`Job ${key} has finished with exit code ${retCode}. Cleaning up now...`);
                const jobWorkingDir = pathUtils.resolve(workingDir, key);
                const statusFilePath = pathUtils.resolve(jobWorkingDir, JOB_FILE);

                const cmd = `cd ${jobWorkingDir} ; mv iiat/COMPILE-OUTPUT.txt iiat/COMPILE-ERROR.txt iiat/COMPILE-TIME.txt iiat/OUTPUT.txt iiat/ERROR.txt iiat/TIME.txt .`; // ADD THIS TO CLEAN UP ; rm -rf iiat`
                exec(cmd);

                finishedJobs.push(key);
                runningProcesses.delete(key);

                if(retCode === 0) 
                    changeJobStatus(statusFilePath, "COMPLETED");
                else
                    changeJobStatus(statusFilePath, "ERROR");
            }
        });
    } // Don't report error message. It was done in the searchForNewJobs function already.
}

function cancelJobs() {
    if(workingDir && fs.existsSync(workingDir)) {
        fs.readdirSync(workingDir, { withFileTypes: true })
            .filter(dirent => dirent.isDirectory())
            .map(dirent => {
                let name = dirent.name;
                let jobDir = pathUtils.resolve(workingDir, name);
                if(fs.existsSync(pathUtils.resolve(jobDir, CANCEL_FILENAME))) {
                    const statusFilePath = pathUtils.resolve(jobDir, JOB_FILE);
                    changeJobStatus(statusFilePath, "CANCELED");
                    const proc = runningProcesses.get(name);
                    if(proc) {
                        proc.kill();
                        runningProcesses.delete(name);
                    }
                    jobQ.forEach((element, index) => {
                        if(element === name) {
                            jobQ.splice(index, 1);
                        }
                    });
                    finishedJobs.push(name);
                }

            });
    } else {
        console.error("Unable to find working directory! (workingDir = " + workingDir + ")");
    }
}

function deleteJobs() {
    if(workingDir && fs.existsSync(workingDir)) {
        fs.readdirSync(workingDir, { withFileTypes: true })
            .filter(dirent => dirent.isDirectory())
            .map(dirent => {
                let name = dirent.name;
                let jobDir = pathUtils.resolve(workingDir, name);
                if(fs.existsSync(pathUtils.resolve(jobDir, DELETE_FILENAME))) {
                    const proc = runningProcesses.get(name);
                    if(proc) {
                        proc.kill();
                        runningProcesses.delete(name);
                    }
                    jobQ.forEach((element, index) => {
                        if(element === name) {
                            jobQ.splice(index, 1);
                        }
                    });
                    finishedJobs.forEach((element, index) => {
                        if(element === name) {
                            finishedJobs.splice(index, 1);
                        }
                    });
                    fs.rmSync(jobDir, {recursive: true, force: true});
                }

            });
    } else {
        console.error("Unable to find working directory! (workingDir = " + workingDir + ")");
    }
}


setInterval(() => {
    searchForNewJobs();
    finishRunningJobs();
    startJobs();
    cancelJobs();
    deleteJobs();
}, 5000);