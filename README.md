This is a docker containerization of the Implicit Interactions Analysis Tool (https://gitlab.com/CyberSEA-Public/ImplicitInteractionsAnalysisTool) with a RESTful web api to interact with it.

# Quick start

# Developer Notes

## GIT 

This project contains git submodules. To fully check out the project, issue the following git commands: 

`git clone git@gitlab.com:uiuc-iti/implicit-interactions-analysis-tool-container.git
git submodule init
git submodule update
`

## Docker

### Building the container image

This section is ONLY if you need to build the docker image from scratch. If you just need to run a container with the IIAT web-api, skip to the next section and it will pull the latest image from dockerhub.

To build this docker image (from the project root directory), issue the following command:

`docker build -t uiuciti/iiat .`

### Running the IIAT web-api container

You can run the container with the following command:

`docker run --name iiat -p 8080:8080 -d uiuciti/iiat`

The web-api is accessible on port 8080 by default. If you need it to run on a different port, you can use this command instead:

`docker run --name iiat -p <PORTNUM>:8080 -d uiuciti/iiat`

To run the container and attach a terminal (useful for debugging), use this command: 

`docker run -it -p 8080:8080 uiuciti/iiat bash`

Once the container has started, you can launch the job-manager and web-api by executing:

`./bootstrap.sh`

If you are running another CPU architecture, the IIAT stack requires an amd64 environment. This can be emulated by docker by adding this argument to the docker run commands: 

`--platform=linux/amd64`

