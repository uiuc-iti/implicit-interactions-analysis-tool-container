#!/bin/bash

IIAT_DIR="/usr/local/iiat/iiat"
GEN_LHS_PY="/usr/local/iiat/gen-lhs.py"

cd $1
cp ${GEN_LHS_PY} .
cp -r ${IIAT_DIR} ./iiat

python3 gen-lhs.py ./project.json Input > Input.lhs
cp Input.lhs iiat/src

cp *.txt iiat/specs/input/

cd iiat

time ( sh -c 'make clean; make' > COMPILE-OUTPUT.txt 2> COMPILE-ERROR.txt ) 2> COMPILE-TIME.txt
time ( ./ImplicitInteractionsAnalysisTool > OUTPUT.txt 2> ERROR.txt ; ) 2> TIME.txt
