
import { PathItemObject } from "openapi3-ts";
import { GET_JOBS_PATH } from "../../../routes/job/getJobs";

export const getGetJobsRouteSchema = () => {
    const path = GET_JOBS_PATH;
    const pio : PathItemObject = {
        get: {
            description: "Get the set of jobs",
            summary: "Get the set of jobs that currently exist in the workspace.",
            operationId: "get-jobs",
            tags: ["Jobs"],
            responses: {
                '200': {
                    $ref: "#/components/schemas/job"
                }
            }
        }
    };

    return [path, pio] as [string, PathItemObject];
};

