
import { getCreateJobRouteSchema } from "./createJobSchema";
import { getGetJobsRouteSchema } from "./getJobsSchema";

import { OpenApiBuilder, PathItemObject } from "openapi3-ts"


export const initJobRoutesSchema = (builder : OpenApiBuilder) => {

    const pathsMap = new Map();

    addPathItem(pathsMap, ...getCreateJobRouteSchema());
    addPathItem(pathsMap, ...getGetJobsRouteSchema());

    pathsMap.forEach((val : PathItemObject[], key) => {
        let pio : PathItemObject = {}
        val.forEach((v) => {
            pio = {
                ...pio,
                ...v
            };
        })

        builder.addPath(key, pio);
    });
}

function addPathItem(pathsMap : Map<string, PathItemObject[]>, path : string, pi : PathItemObject) {
    if(pathsMap.has(path))
        pathsMap.get(path)?.push(pi);
    else pathsMap.set(path, [pi]);
}