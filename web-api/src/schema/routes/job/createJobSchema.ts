
import { PathItemObject } from "openapi3-ts";
import { CREATE_JOB_PATH } from "../../../routes/job/createJob";


export const getCreateJobRouteSchema = () => {

    const path = CREATE_JOB_PATH;
    const pio : PathItemObject = {   post: {
            description: "Create a new job",
            summary: "Creates a new job in the workspace.",
            operationId: "create-job",
            tags: ["Jobs"],
            requestBody: {
                content: {
                    "application/json": {
                        schema: {
                            properties: {
                                project_json: {
                                    description: "The project json file contents.",
                                    required: ["true"],
                                    format: "textarea",
                                    $ref: "#/components/schemas/project_json"
                                },
                                agents: {
                                    description: "The JSON map of all agent specifications.",
                                    required: ["true"],
                                    example: JSON.stringify(exampleAgentsJson),
                                    format: "textarea"
                                }
                            }
                        }
                            
                    }
                }
            },
            responses: {
                '200': {$ref: "#/components/schemas/job"}
            }
        }
    };

    return [path, pio] as [string, PathItemObject];
};



const exampleAgentsJson = {
    C : `begin AGENT where
    C := IDLE + LOAD + UNLOAD +  PREP + INIT + PROC + RESETH + RESETP
    end
    begin NEXT_BEHAVIOUR where
    
    (start,IDLE)  = LOAD
    (start,LOAD)  = LOAD
    (start,PREP)  = PREP
    (start,UNLOAD) = UNLOAD
    (start,INIT)  = INIT
    (start,PROC)  = PROC
    (start,RESETH) = RESETH
    (start,RESETP) = RESETP
    
    (load,IDLE)  = IDLE
    (load,LOAD)  = LOAD
    (load,PREP)  = PREP
    (load,UNLOAD) = UNLOAD
    (load,INIT)  = INIT
    (load,PROC)  = PROC
    (load,RESETH) = RESETH
    (load,RESETP) = RESETP
    
    (loaded,IDLE)  = IDLE
    (loaded,LOAD)  = PREP
    (loaded,PREP)  = PREP
    (loaded,UNLOAD) = UNLOAD
    (loaded,INIT)  = INIT
    (loaded,PROC)  = PROC
    (loaded,RESETH) = RESETH
    (loaded,RESETP) = RESETP
    
    (prepare,IDLE)  = IDLE
    (prepare,LOAD)  = LOAD
    (prepare,PREP)  = PREP
    (prepare,UNLOAD) = UNLOAD
    (prepare,INIT)  = INIT
    (prepare,PROC)  = PROC
    (prepare,RESETH) = RESETH
    (prepare,RESETP) = RESETP
    
    (prepared,IDLE)  = IDLE
    (prepared,LOAD)  = LOAD
    (prepared,PREP)  = UNLOAD
    (prepared,UNLOAD)  = UNLOAD
    (prepared,INIT)  = INIT
    (prepared,PROC)  = PROC
    (prepared,RESETH) = RESETH
    (prepared,RESETP) = RESETP
    
    (unload,IDLE)  = IDLE
    (unload,LOAD)  = LOAD
    (unload,PREP)  = PREP
    (unload,UNLOAD) = UNLOAD
    (unload,INIT)  = INIT
    (unload,PROC)  = PROC
    (unload,RESETH) = RESETH
    (unload,RESETP) = RESETP
    
    (unloaded,IDLE)  = IDLE
    (unloaded,LOAD)  = LOAD
    (unloaded,PREP)  = PREP
    (unloaded,UNLOAD) = INIT
    (unloaded,INIT)  = INIT
    (unloaded,PROC)  = PROC
    (unloaded,RESETH) = RESETH
    (unloaded,RESETP) = RESETP
    
    (setup,IDLE)  = IDLE
    (setup,LOAD)  = LOAD
    (setup,PREP)  = PREP
    (setup,UNLOAD) = UNLOAD
    (setup,INIT)  = INIT
    (setup,PROC)  = PROC
    (setup,RESETH) = RESETH
    (setup,RESETP) = RESETP
    
    (ready,IDLE)  = IDLE
    (ready,LOAD)  = LOAD
    (ready,PREP)  = PREP
    (ready,UNLOAD) = UNLOAD
    (ready,INIT)  = PROC
    (ready,PROC)  = PROC
    (ready,RESETH) = RESETH
    (ready,RESETP) = RESETP
    
    (process,IDLE)  = IDLE
    (process,LOAD)  = LOAD
    (process,PREP)  = PREP
    (process,UNLOAD) = UNLOAD
    (process,INIT)  = INIT
    (process,PROC)  = PROC
    (process,RESETH) = RESETH
    (process,RESETP) = RESETP
    
    (processed,IDLE)  = IDLE
    (processed,LOAD)  = LOAD
    (processed,PREP)  = PREP
    (processed,UNLOAD) = UNLOAD
    (processed,INIT)  = INIT
    (processed,PROC)  = RESETH
    (processed,RESETH) = RESETH
    (processed,RESETP) = RESETP
    
    (resetH,IDLE) = IDLE
    (resetH,LOAD) = LOAD
    (resetH,PREP) = PREP
    (resetH,UNLOAD) = UNLOAD
    (resetH,INIT) = INIT
    (resetH,PROC) = PROC
    (resetH,RESETH) = RESETH
    (resetH,RESETP) = RESETP
    
    (doneH,IDLE) = IDLE
    (doneH,LOAD) = LOAD
    (doneH,PREP) = PREP
    (doneH,UNLOAD) = UNLOAD
    (doneH,INIT) = INIT
    (doneH,PROC) = PROC
    (doneH,RESETH)  = RESETP
    (doneH,RESETP) = RESETP
    
    (resetP,IDLE) = IDLE
    (resetP,LOAD) = LOAD
    (resetP,PREP) = PREP
    (resetP,UNLOAD) = UNLOAD
    (resetP,INIT) = INIT
    (resetP,PROC) = PROC
    (resetP,RESETH) = RESETH
    (resetP,RESETP) = RESETP
    
    (doneP,IDLE) = IDLE
    (doneP,LOAD) = LOAD
    (doneP,PREP) = PREP
    (doneP,UNLOAD) = UNLOAD
    (doneP,INIT) = INIT
    (doneP,PROC) = PROC
    (doneP,RESETH) = RESETH
    (doneP,RESETP)  = IDLE
    
    (end,IDLE)  = IDLE
    (end,LOAD)  = LOAD
    (end,PREP)  = PREP
    (end,UNLOAD) = UNLOAD
    (end,INIT)  = INIT
    (end,PROC)  = PROC
    (end,RESETH) = RESETH
    (end,RESETP) = RESETP
    
    end
    begin NEXT_STIMULUS where
    
    (start,IDLE)  = load
    (start,LOAD)  = N
    (start,PREP)  = N
    (start,UNLOAD) = N
    (start,INIT)  = N
    (start,PROC)  = N
    (start,RESETH) = N
    (start,RESETP) = N
    
    (load,IDLE)  = N
    (load,LOAD)  = N
    (load,PREP)  = N
    (load,UNLOAD) = N
    (load,INIT)  = N
    (load,PROC)  = N
    (load,RESETH) = N
    (load,RESETP) = N
    
    (loaded,IDLE)  = N
    (loaded,LOAD)  = prepare
    (loaded,PREP)  = N
    (loaded,UNLOAD) = N
    (loaded,INIT)  = N
    (loaded,PROC)  = N
    (loaded,RESETH) = N
    (loaded,RESETP) = N
    
    (prepare,IDLE)  = N
    (prepare,LOAD)  = N
    (prepare,PREP)  = N
    (prepare,UNLOAD) = N
    (prepare,INIT)  = N
    (prepare,PROC)  = N
    (prepare,RESETH) = N
    (prepare,RESETP) = N
    
    (prepared,IDLE)  = N
    (prepared,LOAD)  = N
    (prepared,PREP)  = unload
    (prepared,UNLOAD) = N
    (prepared,INIT)  = N
    (prepared,PROC)  = N
    (prepared,RESETH) = N
    (prepared,RESETP) = N
    
    (unload,IDLE)  = N
    (unload,LOAD)  = N
    (unload,PREP)  = N
    (unload,UNLOAD) = N
    (unload,INIT)  = N
    (unload,PROC)  = N
    (unload,RESETH) = N
    (unload,RESETP) = N
    
    (unloaded,IDLE)  = N
    (unloaded,LOAD)  = N
    (unloaded,PREP)  = setup
    (unloaded,UNLOAD) = N
    (unloaded,INIT)  = N
    (unloaded,PROC)  = N
    (unloaded,RESETH) = N
    (unloaded,RESETP) = N
    
    (setup,IDLE)  = N
    (setup,LOAD)  = N
    (setup,PREP)  = N
    (setup,UNLOAD) = N
    (setup,INIT)  = N
    (setup,PROC)  = N
    (setup,RESETH) = N
    (setup,RESETP) = N
    
    (ready,IDLE)  = N
    (ready,LOAD)  = N
    (ready,PREP)  = N
    (ready,UNLOAD) = N
    (ready,INIT)  = process
    (ready,PROC)  = N	
    (ready,RESETH) = N
    (ready,RESETP) = N
    
    (process,IDLE)  = N
    (process,LOAD)  = N
    (process,PREP)  = N
    (process,UNLOAD) = N
    (process,INIT)  = N
    (process,PROC)  = N
    (process,RESETH) = N
    (process,RESETP) = N
    
    (processed,IDLE)  = N
    (processed,LOAD)  = N
    (processed,PREP)  = N
    (processed,UNLOAD) = N
    (processed,INIT)  = N
    (processed,PROC)  = resetH
    (processed,RESETH) = N
    (processed,RESETP) = N
    
    (resetH,IDLE) = N
    (resetH,LOAD) = N
    (resetH,PREP) = N
    (resetH,UNLOAD) = N
    (resetH,INIT) = N
    (resetH,PROC) = N
    (resetH,RESETH) = N
    (resetH,RESETP) = N
    
    (doneH,IDLE) = N
    (doneH,LOAD) = N
    (doneH,PREP) = N
    (doneH,UNLOAD) = N
    (doneH,INIT) = N
    (doneH,PROC) = N
    (doneH,RESETH)  = resetP
    (doneH,RESETP) = N
    
    (resetP,IDLE) = N
    (resetP,LOAD) = N
    (resetP,PREP) = N
    (resetP,UNLOAD) = N
    (resetP,INIT) = N
    (resetP,PROC) = N
    (resetP,RESETH) = N
    (resetP,RESETP) = N
    
    (doneP,IDLE) = N
    (doneP,LOAD) = N
    (doneP,PREP) = N
    (doneP,UNLOAD) = N
    (doneP,INIT) = N
    (doneP,PROC) = N
    (doneP,RESETH) = N
    (doneP,RESETP)  = end
    
    (end,IDLE)  = N
    (end,LOAD)  = N
    (end,PREP)  = N
    (end,UNLOAD) = N
    (end,INIT)  = N
    (end,PROC)  = N		
    (end,RESETH) = N
    (end,RESETP) = N
    
    end
    begin CONCRETE_BEHAVIOUR where
    IDLE => [ state := 0;
            hHasMaterial := 0 ]
    LOAD => [ state := 1 ]
    PREP => [ state := 2;
            sFull := 1]
    UNLOAD => [ state := 3]
    INIT => [ state := 4;
            sFull := 0;
            hHasMaterial := 1]
    PROC => [ state := 5 ]
    RESETH => [ state := 6;
            hHasMaterial := 0  ]
    RESETP => [ state := 7 ]
    end`,
    H : `begin AGENT where
    H := WAIT + MOVE
    end
    begin NEXT_BEHAVIOUR where
    
    (start,WAIT)  = WAIT
    (start,MOVE)  = MOVE
    
    (load,WAIT)  = WAIT
    (load,MOVE)  = MOVE
    
    (loaded,WAIT)  = WAIT
    (loaded,MOVE)  = MOVE
    
    (prepare,WAIT)  = MOVE
    (prepare,MOVE)  = MOVE
    
    (prepared,WAIT)  = WAIT
    (prepared,MOVE)  = MOVE
    
    (unload,WAIT)  = WAIT
    (unload,MOVE)  = MOVE
    
    (unloaded,WAIT)  = WAIT
    (unloaded,MOVE)  = MOVE
    
    (setup,WAIT)  = WAIT
    (setup,MOVE)  = MOVE
    
    (ready,WAIT)  = WAIT
    (ready,MOVE)  = MOVE
    
    (process,WAIT)  = WAIT
    (process,MOVE)  = MOVE
    
    (processed,WAIT)  = WAIT
    (processed,MOVE)  = MOVE
    
    (resetH,WAIT)  = WAIT
    (resetH,MOVE)  = WAIT
    
    (doneH,WAIT) = WAIT
    (doneH,MOVE) = MOVE
    
    (resetP,WAIT) = WAIT
    (resetP,MOVE) = MOVE
    
    (doneP,WAIT) = WAIT
    (doneP,MOVE) = MOVE
    
    (end,WAIT)  = WAIT
    (end,MOVE)  = MOVE
    
    end
    begin NEXT_STIMULUS where
    
    (start,WAIT)  = N
    (start,MOVE)  = N
    
    (load,WAIT)  = N
    (load,MOVE)  = N
    
    (loaded,WAIT)  = N
    (loaded,MOVE)  = N
    
    (prepare,WAIT)  = prepared 
    (prepare,MOVE)  = N
    
    (prepared,WAIT)  = N 
    (prepared,MOVE)  = N
    
    (unload,WAIT)  = N
    (unload,MOVE)  = N
    
    (unloaded,WAIT)  = N
    (unloaded,MOVE)  = N
    
    (setup,WAIT)  = N
    (setup,MOVE)  = N
    
    (ready,WAIT)  = N
    (ready,MOVE)  = N
    
    (process,WAIT)  = N
    (process,MOVE)  = N
    
    (processed,WAIT)  = N
    (processed,MOVE)  = N
    
    (resetH,WAIT)  = N
    (resetH,MOVE) = doneH
    
    (doneH,WAIT) = N
    (doneH,MOVE) = N
    
    (resetP,WAIT) = N
    (resetP,MOVE) = N
    
    (doneP,WAIT) = N
    (doneP,MOVE) = N
    
    (end,WAIT)  = N
    (end,MOVE)  = N
    
    end
    begin CONCRETE_BEHAVIOUR where
    WAIT => [ skip ]
    MOVE => [ if  (sFull = 1) -> material := GETMATERIAL 
               | ~(sFull = 1) -> material := NULL
              fi  ]	
    end`,
    P : `begin AGENT where
    P := STBY + SET + WORK
    end
    begin NEXT_BEHAVIOUR where
    
    (start,STBY)      = STBY
    (start,SET)       = SET
    (start,WORK)      = WORK
    
    (load,STBY)       = STBY
    (load,SET)        = SET
    (load,WORK)       = WORK
    
    (loaded,STBY)     = STBY
    (loaded,SET)      = SET
    (loaded,WORK)     = WORK
    
    (prepare,STBY)       = STBY
    (prepare,SET)        = SET
    (prepare,WORK)       = WORK
    
    (prepared,STBY) = STBY
    (prepared,SET) = SET
    (prepared,WORK) = WORK
    
    (unload,STBY)     = STBY
    (unload,SET)      = SET
    (unload,WORK)     = WORK
    
    (unloaded,STBY)   = STBY
    (unloaded,SET)    = SET
    (unloaded,WORK)   = WORK
    
    (setup,STBY)      = SET
    (setup,SET)       = SET
    (setup,WORK)      = WORK
    
    (ready,STBY)      = STBY
    (ready,SET)       = SET
    (ready,WORK)      = WORK
    
    (process,STBY)    = STBY
    (process,SET)     = WORK
    (process,WORK)    = WORK
    
    (processed,STBY)  = STBY
    (processed,SET)   = SET
    (processed,WORK)  = WORK
    
    (resetH,STBY) = STBY
    (resetH,SET) = SET
    (resetH,WORK) = WORK
    
    (doneH,STBY) = STBY
    (doneH,SET) = SET
    (doneH,WORK) = WORK
    
    (resetP,STBY) = STBY
    (resetP,SET) = SET
    (resetP,WORK) = STBY
    
    (doneP,STBY) = STBY
    (doneP,SET) = SET
    (doneP,WORK) = WORK
    
    (end,STBY)      = STBY
    (end,SET)       = SET
    (end,WORK)      = WORK
    
    end
    begin NEXT_STIMULUS where
    
    (start,STBY)     = N
    (start,SET)      = N
    (start,WORK)     = N
    
    (load,STBY)      = N
    (load,SET)       = N
    (load,WORK)      = N
    
    (loaded,STBY)    = N
    (loaded,SET)     = N
    (loaded,WORK)    = N
    
    (prepare,STBY)      = N
    (prepare,SET)       = N
    (prepare,WORK)      = N
    
    (prepared,STBY) = N
    (prepared,SET) = N
    (prepared,WORK) = N
    
    (unload,STBY)    = N
    (unload,SET)     = N
    (unload,WORK)    = N
    
    (unloaded,STBY)  = N
    (unloaded,SET)   = N
    (unloaded,WORK)  = N
    
    (setup,STBY)     = ready
    (setup,SET)      = N
    (setup,WORK)     = N
    
    (ready,STBY)     = N
    (ready,SET)      = N
    (ready,WORK)     = N	
    
    (process,STBY)   = N
    (process,SET)    = processed
    (process,WORK)   = N
    
    (processed,STBY) = N
    (processed,SET)  = N
    (processed,WORK) = N
    
    (resetH,STBY) = N
    (resetH,SET) = N
    (resetH,WORK) = N
    
    (doneH,STBY) = N
    (doneH,SET) = N
    (doneH,WORK) = N
    
    (resetP,STBY) = N
    (resetP,SET) = N
    (resetP,WORK) = doneP
    
    (doneP,STBY) = N
    (doneP,SET) = N
    (doneP,WORK) = N
    
    (end,STBY)     = N
    (end,SET)      = N
    (end,WORK)     = N
    
    end
    begin CONCRETE_BEHAVIOUR where
    STBY => [ skip ]
    SET  => [ if  (hHasMaterial = 1 && state = 4 && sFull = 0)  -> ready := 1
                | ~(hHasMaterial = 1 && state = 4 && sFull = 0)  -> ready := 0
              fi ]	
    WORK => [ if  (ready = 1)  -> part := PROCESS
                | ~(ready = 1)  -> part := NULL
              fi ]	
    end`,
    S : `begin AGENT where
    S := EMPTY + FULL
    end
    begin NEXT_BEHAVIOUR where
    
    (start,EMPTY)  = EMPTY
    (start,FULL) = FULL
    (load,EMPTY)  = FULL
    (load,FULL) = FULL
    (loaded,EMPTY)  = EMPTY
    (loaded,FULL) = FULL
    (prepare,EMPTY)  = EMPTY
    (prepare,FULL) = FULL
    (prepared,EMPTY) = EMPTY
    (prepared,FULL) = FULL
    (unload,EMPTY)  = EMPTY
    (unload,FULL) = EMPTY
    (unloaded,EMPTY)  = EMPTY
    (unloaded,FULL) = FULL
    (setup,EMPTY)  = EMPTY
    (setup,FULL) = FULL
    (ready,EMPTY)  = EMPTY
    (ready,FULL) = FULL
    (process,EMPTY)  = EMPTY
    (process,FULL) = FULL
    (processed,EMPTY)  = EMPTY
    (processed,FULL) = FULL
    (resetH,EMPTY) = EMPTY
    (resetH,FULL) = FULL
    (doneH,EMPTY) = EMPTY
    (doneH,FULL) = FULL
    (resetP,EMPTY) = EMPTY
    (resetP,FULL) = FULL
    (doneP,EMPTY) = EMPTY
    (doneP,FULL) = FULL
    (end,EMPTY)  = EMPTY
    (end,FULL) = FULL
    
    end
    begin NEXT_STIMULUS where
    
    (start,EMPTY)  = N
    (start,FULL) = N
    (load,EMPTY)  = loaded
    (load,FULL) = N
    (loaded,EMPTY)  = N
    (loaded,FULL) = N
    (prepare,EMPTY)  = N
    (prepare,FULL) = N
    (prepared,EMPTY) = N
    (prepared,FULL) = N
    (unload,EMPTY)  = N
    (unload,FULL) = unloaded
    (unloaded,EMPTY)  = N
    (unloaded,FULL) = N
    (setup,EMPTY)  = N
    (setup,FULL) = N
    (ready,EMPTY)  = N
    (ready,FULL) = N
    (process,EMPTY)  = N
    (process,FULL) = N
    (processed,EMPTY)  = N
    (processed,FULL) = N
    (resetH,EMPTY) = N
    (resetH,FULL) = N
    (doneH,EMPTY) = N
    (doneH,FULL) = N
    (resetP,EMPTY) = N
    (resetP,FULL) = N
    (doneP,EMPTY) = N
    (doneP,FULL) = N
    (end,EMPTY)  = N
    (end,FULL) = N
    
    end
    begin CONCRETE_BEHAVIOUR where
    EMPTY => [ skip ]
    FULL  => [ skip ]
    end`
};



