import { OpenApiBuilder } from "openapi3-ts";

export const addProject_jsonSchema = (builder : OpenApiBuilder) => {
    builder.addSchema("project_json", {
        description: "The project definition file that describes the agent specifications.",
        example: exampleProjectJson,
        properties: {
            edges: {
                type: 'array',
                items: {
                    type: 'array',
                    items: {
                        oneOf: [
                            {
                                type: 'string',
                            },
                            {
                                type: 'array',
                                items: {
                                    oneOf: [
                                        {type: 'string'},
                                        {type: 'number'}
                                    ]
                                }
                            }
                        ]
                    }, 
                    minItems: 3,
                    maxItems: 3,
                        
                }
            },
            stim_set: {
                type: 'array',
                items: {
                    type: 'string'
                }
            },
            cka_set: {
                type: 'array',
                items: {
                    type: 'string'
                }
            },
            constants: {
                type: 'array',
                items: {
                    type: 'string'
                }
            },
            starting_agent: {
                type: 'array',
                items: {
                    type: 'array',
                    items: {
                        oneOf: [
                            {type: 'number'},
                            {type: 'string'}
                        ]
                    }
                }
            },
            external_stim: {
                type: 'array',
                items: {
                    type: 'string'
                }
            }
        }
    });
}


const exampleProjectJson = `{
    "edges" : 	[[[1,"C"],[2,"S"],"S"],
          [[2,"S"],[3,"C"],"S"],
          [[3,"C"],[4,"H"],"B"],
          [[4,"H"],[5,"C"],"S"],
          [[5,"C"],[6,"S"],"S"],
          [[6,"S"],[7,"C"],"S"],
          [[7,"C"],[8,"P"],"B"],
          [[8,"P"],[9,"C"],"S"],
          [[9,"C"],[10,"P"],"B"],
          [[10,"P"],[11,"C"],"S"],
          [[11,"C"],[12,"H"],"S"],
          [[12,"H"],[13,"C"],"S"],
          [[13,"C"],[14,"P"],"S"],
          [[14,"P"],[15,"C"],"S"]],
          
    "stim_set" :["start", "load", "loaded", "prepare", "prepared", "unload", "unloaded", "setup", "ready", "process", "processed", "resetH", "doneH", "resetP", "doneP", "end", "D", "N"],
    
    "cka_set" : ["IDLE", "LOAD", "PREP", "UNLOAD", "INIT", "PROC", "RESETH", "RESETP", "WAIT", "MOVE", "STBY", "SET", "WORK", "EMPTY", "FULL", "0", "1"],
    
    "constants" : ["PROCESS", "GETMATERIAL", "NULL"],
    
    "starting_agent" : [[1,"C"]],
    
    "external_stim" : ["start"]
  }
  `;

