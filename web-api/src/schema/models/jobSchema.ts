

import { OpenApiBuilder } from "openapi3-ts";
import { JobStatus } from "../../models/jobStatusEnum"

export const addJobSchema = (builder : OpenApiBuilder) => {
    builder.addSchema("job", {
        description: "Job",
        properties: {
            _specs: {
                properties: {
                    dataType: {type: 'string', description: 'Must be \"Map\".'},
                    value: {
                        type: 'array', 
                        description: 'Array of key/value pairs.',
                        items: {
                            type: 'array', 
                            items: {type: 'string'},
                            minItems: 2, 
                            maxItems: 2,
                            description: 'Array of length two with the key in the 0th position and the value in the 1st.'
                        }
                    }
                },
                description: 'A map with names of agent specs as keys and the spec content as value.'
            },
            _usingTmpDir: {type: 'boolean', description: 'Should always be false.'},
            _id: {type: 'string', format: 'uuid', description: 'Unique id for the job.'},
            _status: {type: 'string', enum: Object.values(JobStatus), description: 'The current status of the job.'},
            _projectJson: {type: 'string', description: 'The project\'s JSON file content.'}
        }
    });

}
