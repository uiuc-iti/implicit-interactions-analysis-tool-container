import { Application } from "express";
import { OpenApiBuilder } from "openapi3-ts"
import swaggerUi from "swagger-ui-express";

import { addJobSchema } from "./models/jobSchema";
import { addProject_jsonSchema } from "./models/project_jsonSchema";
import { initJobRoutesSchema } from "./routes/job/initJobSchema";

export const initSchema = (app: Application) => {

    const builder = OpenApiBuilder.create()
        .addVersion("v1.0")
        .addTitle("IIAT Job Management API")
        .addDescription("This API allows for the creation and management of IIAT jobs.")
        .addContact({
            email: "kjkeefe@illinois.edu",
            name: "Ken Keefe",
            url: "http://www.perform.illinois.edu/~kjkeefe"
        })
        .addServer({
            url: "http://localhost:8080"
        })
        .addLicense({
            name: "MIT License",
            url: "https://opensource.org/licenses/MIT"
        });


    // Initialize models
    addJobSchema(builder);
    addProject_jsonSchema(builder);

    // Initialize routes
    initJobRoutesSchema(builder);

    const openApiJson = builder.getSpecAsJson();

    app.get("/openapi.json", function(req, res) {
        res.json(openApiJson);
    });

    app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(JSON.parse(openApiJson)));


}