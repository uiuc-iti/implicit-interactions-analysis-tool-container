import cluster from "cluster";
import startServer from "./server";

function startWorker() {
    const worker = cluster.fork();
    console.log(`CLUSTER: Worker ${worker.id} started.`);
}

if(cluster.isPrimary) {
    require('os').cpus().forEach(startWorker);

    cluster.on('disconnect', worker => console.log(`CLUSTER: Worker ${worker.id} disconnected from the cluster.`));

    cluster.on('exit', (worker, code, signal) => {
        console.log(`CLUSTER: Worker ${worker.id} died with exit code ${code} (${signal})`);
        startWorker();
    });
} else {
    startServer(parseInt(process.env.SERVER_PORT || "3000"))
}

