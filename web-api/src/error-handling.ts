import { Express } from "express";

function configureErrorHandling(app : Express) {
    process.on('uncaughtException', err => {
        console.error('UNCAUGHT EXCEPTION\n', err.stack);
        process.exit(1);
    });
    
};


export default configureErrorHandling;