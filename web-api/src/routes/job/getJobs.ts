import express from "express";
import Job from "../../models/job";

export const GET_JOBS_PATH = "/v1/jobs";

export default function getJobsRoute(router: express.Router) {

    // Get the list of jobs
    router.get(GET_JOBS_PATH, (req, res, next) => {
        const jobs = Job.GetJobs();
        res.send(Array.from(jobs));
        next();
    });
}