
import express, { Express } from "express";
import Job from "../../models/job";
import createJobRoute from "./createJob";
import getJobsRoute from "./getJobs";

function configureJobRouting(app: Express, router: express.Router) {

    createJobRoute(router);
    getJobsRoute(router);

    interface ISpecificJobRequest extends express.Request {
        params: {
            jobId: string // The id of the job
        }
    }

    // Get information about a specific job
    router.get("/v1/jobs/:jobId", (req: ISpecificJobRequest, res, next) => {
        const job = checkJobExists(req.params.jobId, res);
        if (job) {
            res.send(job);
        }
        next();
    });

    // Get the input spec file for a specific job and agent 
    router.get("/v1/jobs/:jobId/input/:spec", (req, res, next) => {
        const job = checkJobExists(req.params.jobId, res);
        if (job) {
            const spec = job.getSpec(req.params.spec);
            if (spec)
                res.send(spec);
            else {
                res.status(404).send("Unable to fine spec in job.");
                res.end();
            }
        }
        next();
    });

    // Get the job's json specification
    router.get("/v1/jobs/:jobId/input", (req, res, next) => {
        const job = checkJobExists(req.params.jobId, res);
        if (job) {
            res.send(job.projectJson);
        }
        next();
    });

    // Get the compiled tool's stdout for a specific job
    router.get("/v1/jobs/:jobId/output", (req, res, next) => {
        const job = checkJobExists(req.params.jobId, res);
        if (job) {
            res.send(job.output);
        }
        next();
    });

    // Get the compiled tool's timing output for a specific job
    router.get("/v1/jobs/:jobId/time", (req, res, next) => {
        const job = checkJobExists(req.params.jobId, res);
        if (job) {
            res.send(job.time);
        }
        next();
    });

    // Get the compiled tool's stderr for a specific job
    router.get("/v1/jobs/:jobId/error", (req, res, next) => {
        const job = checkJobExists(req.params.jobId, res);
        if (job) {
            res.send(job.error);
        }
        next();
    });

    // Get the compile stdout for the job
    router.get("/v1/jobs/:jobId/compile-output", (req, res, next) => {
        const job = checkJobExists(req.params.jobId, res);
        if (job) {
            res.send(job.compileOutput);
        }
        next();
    });

    // Get the compile stderr for the job.
    router.get("/v1/jobs/:jobId/compile-error", (req, res, next) => {
        const job = checkJobExists(req.params.jobId, res);
        if (job) {
            res.send(job.compileError);
        }
        next();
    });

    // Get the compile timing output for the job
    router.get("/v1/jobs/:jobId/compile-time", (req, res, next) => {
        const job = checkJobExists(req.params.jobId, res);
        if (job) {
            res.send(job.compileTime);
        }
        next();
    });

    // Cancel the job
    router.patch("/v1/jobs/:jobId/cancel", (req, res, next) => {
        const job = checkJobExists(req.params.jobId, res);
        if (job) {
            job.cancel();
        }
        next();
    });

    // Delete the job
    router.delete("/v1/jobs/:jobId/cancel", (req, res, next) => {
        const job = checkJobExists(req.params.jobId, res);
        if (job) {
            job.delete();
        }
        next();
    });

}

function checkJobExists(jobId: string, res: express.Response): Job | undefined {
    const job = Job.GetJob(jobId);
    if (job === undefined) {
        res.status(404);
        res.end();
    }
    return job;
}

export default configureJobRouting;