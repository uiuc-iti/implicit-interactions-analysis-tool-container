
import express from "express";
import Job from "../../models/job";
import bodyParser from "body-parser";


export const CREATE_JOB_PATH = "/v1/jobs";

export default function createJobRoute(router: express.Router) {
    
    // Create a new job
    router.post(CREATE_JOB_PATH, bodyParser.json(), async (req, res, next) => {
        const job = Job.GetJob();
        let err : string | undefined = undefined;
        if (job) {
            if(req.body) {
                if('project_json' in req.body) {
                    job.setProjectJson(req.body.project_json);
                } else err = "Missing project_json property.";

                if(err === undefined && 'agents' in req.body) {
                    for(const [k, v] of Object.entries(req.body.agents)) {
                        if(typeof v === "string") 
                            job.setSpec(k, v);
                    }
                    job.completeInitialization();
                    res.send(job.toString());
                } else err = "Missing agents property.";
            } else {
                err = "No input provided during job creation!";
            }
        }

        if(err)
            res.status(400).send(err);

        next();
    });
}