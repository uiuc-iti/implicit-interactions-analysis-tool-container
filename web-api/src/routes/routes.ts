import express, { Express } from "express";
import configureJobRouting from "./job/job-routing";

function configureRouting(app : Express) {
    const router = express.Router();
    app.use(router);

    configureJobRouting(app, router);
    
}




export default configureRouting;