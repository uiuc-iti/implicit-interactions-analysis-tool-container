
/**
 * Enumeration of possible job status states.
 */

export enum JobStatus {
    /**
    * The job is queued and ready to be executed.
    */
    Queued = "Queued",
    /**
    * The job is currently executing.
    */
    Running = "Running",
    /**
    * The job has successfully completed its execution without error.
    */
    Completed = "Completed",
    /**
    * The job has completed and some error has occurred. 
    */
    Error = "Error",
    /**
    * The job has been canceled.
    */
    Canceled = "Canceled"
}
