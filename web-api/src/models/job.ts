import {v4 as uuidv4} from 'uuid';
import { replacer, reviver } from '../util';
import fs from 'fs';
import pathUtils from 'path';
import { JobStatus } from './jobStatusEnum';

/**
 * The main job file containing the json specification of the file. 
 */
const JOB_FILE = "job.json";
/**
 * The project json file (used by gen-lhs)
 */
const PROJECT_JSON_FILE = "project.json";
/**
 * The temporary system location
 */
const TMP_DIR : string = "/tmp";
/**
 * The workspace system location
 */
const WORKSPACE_DIR: string = "/var/local/iiat";

/**
 * Represents an IIAT job and interfaces with the job's working directory and status file on the file system.
 */

class Job {
    /**
     * The id of the job. This is typically a v4 UUID string.
     */
    private _id: string;
    /**
     * The current status. Should be one of the defined options with the prefix JOB_STATUS_.
     */
    private _status : JobStatus;
    /**
     * This field is used when the job is in error. It contains the current error message.
     */
    private _errorMsg: string | undefined;
    /**
     * This field stores a map of agent specifications. The key is the spec name. The value is the spec content.
     */
    private _specs: Map<string, string> = new Map<string, string>();
    /**
     * This field stores whether or not the job is still using the temp directory location or the workspace location.
     */
    private _usingTmpDir : boolean = true;
    /**
     * This field stores the project json specification text.
     */
    private _projectJson : any;

    /**
     * Factory method for creating new jobs or getting an existing job.
     * @param id - The id of an already existing job. If the job is new, do not provide an id and one will be generated.
     * @returns The new or existing job model. Will return undefined if an id is provided, but a job status file either doesn't exist or is corrupted.
     */
    static GetJob(id?: string) : Job | undefined {
        let j = new Job(id);
        if(id && !j.readStatusFileFromJobDirectory()) 
            return undefined; // The status file doesn't exist or is corrupted.
        
        return j;
    }

    /**
     * Gets a set of all jobs found in the workspace.
     */
    static GetJobs() : Set<Job> {
        const ret = new Set<Job>();
        
        // Check all directories existing in the workspace. If you can get a job from the directory name, add it to the set.
        fs.readdirSync(pathUtils.resolve(WORKSPACE_DIR), {withFileTypes: true})
            .filter(dirent => dirent.isDirectory())
            .map(dirent => dirent.name)
            .forEach(jobDir => {
                console.log("Dir: " + jobDir);
                let job = this.GetJob(jobDir);
                console.log("Job: " + job);
                if(job) {
                    ret.add(job);
                }
            });

        return ret;
    }

    /**
     * Constructor for the job model.
     * @param id - The id string (typically a v4 uuid). If one is not provided, a new id will be generated.
     */
    private constructor(id?: string) {
        if(id) {
            this._id = id;
            this._usingTmpDir = false;
        }
        else { // Brand new job!
            this._id = uuidv4();
            this._status = JobStatus.Queued;
            this._errorMsg = undefined;
            this.initializeJobDirectory();
        }
    }

    /**
     * Gets a JSON string representation of the job model.
     * @returns A JSON string representation of the job model.
     */
    toString() : string {
        return JSON.stringify(this, replacer);
    }

    /**
     * Gets the job's status file path.
     */
    get statusFilePath() : string {
        return this.getJobDir() + "/" + JOB_FILE;
    }

    /**
     * Initializes the job's directory in the working directory.
     */
    initializeJobDirectory() : void {
        const statusFilePath = this.statusFilePath;
        fs.mkdirSync(pathUtils.dirname(statusFilePath), {recursive: true});
        console.log("Creating " + statusFilePath);
        this.writeStatusFileFromThis();
    }

    /**
     * Writes the status file using this model. 
     */
    writeStatusFileFromThis() : void {
        fs.writeFileSync(this.statusFilePath, this.toString(), {encoding: "utf-8"});
    }

    /**
     * Reads the status file and sets this model's field values from the file data.
     * @returns True if the read was successful. Otherwise, something went wrong and the model was not initialized.
     */
    readStatusFileFromJobDirectory() : boolean {
        try {
            const statusData = fs.readFileSync(this.statusFilePath, {encoding: "utf-8"});
            const jobModel = JSON.parse(statusData, reviver);
            this._status = jobModel._status;
            this._errorMsg = jobModel._errorMsg;
            this._usingTmpDir = jobModel._usingTmpDir;
            Object.keys(jobModel._specs).forEach(key => {
                this._specs.set(key, jobModel._specs[key]);
            })
            this._projectJson = jobModel._projectJson;
            return true; // Successfully read in and parsed the status file
        } catch (err) {
            if(err instanceof Error) {
                console.log("Error caught: " + err);

            } else {
                console.log("Unknown object caught: " + err);
            }
        }
        return false; // Something went wrong if we get here, so fail.
    }

    /**
     * Set's the job's json specification. This json is used by the gen-lhs script to build the necessary haskell files for executing the job. 
     * @param val The json content that describes the job.
     * @returns A set of spec names found in the job's json text. 
     */
    setProjectJson(json : any) {
        const specNames = new Set<string>();
        
        json.edges.forEach((element: any[]) => {
            specNames.add(element[0][1]);
            specNames.add(element[1][1]);
        });

        const projFile = pathUtils.resolve(this.getJobDir(), PROJECT_JSON_FILE);
        fs.writeFileSync(projFile, JSON.stringify(json));
        this._projectJson = json;

        return specNames;
    }

    get projectJson() : any {
        return this._projectJson;
    }

    /**
     * Sets the agent specification content for a given spec name. 
     * @param specName The name of the agent specification to set
     * @param specContent The new content string value of the agent
     */
    setSpec(specName : string, specContent : string) {
        this._specs.set(specName, specContent);
        const specFile = pathUtils.resolve(this.getJobDir(), specName+".txt");
        fs.writeFileSync(specFile, specContent);
        this.writeStatusFileFromThis();
    }

    /**
     * Gets the specification content of a given spec name.
     * @param specName The name of the agent specification to return
     * @returns The contents of the specification. Returns undefined if the spec does not exist in the job.
     */
    getSpec(specName : string) : string | undefined {
        return this._specs.get(specName);
    }

    /**
     * Get's the set of spec names 
     */
    get specNames() : Set<string> {
        const ret = new Set<string>();
        for(let spec of this._specs.keys())
            ret.add(spec);

        return ret;
    }

    /**
     * Completes the job's initialization by moving the job directory from the temporary initializing location to the workspace.
     */
    completeInitialization() {
        if(this._usingTmpDir) {
            const srcDir = this.getJobDir();
            this._usingTmpDir = false;
            const tgtDir = this.getJobDir();
            fs.renameSync(srcDir, tgtDir);
            this.writeStatusFileFromThis();
        }
    }

    /**
     * The IIAT stdout. This reads the current version from the file system, so if the process is still running, it reads what has been written so far.
     * @returns A string containing the IIAT stdout. Returns undefined if the stdout file doesn't exist or there was an error reading it.
     */
    get output() : string | undefined {
        this.checkJobDirExists();
        const outputFile = this.getJobDir() + "/OUTPUT.txt";
        if(fs.existsSync(outputFile)) {
            try{
                return fs.readFileSync(outputFile, 'utf8');
            } catch (err) {
                console.error(err);
                return undefined;
            }
        } else return undefined;
    }

    /**
     * The IIAT stderr. This reads the current version from the file system, so if the process is still running, it reads what has been written so far.
     * @returns A string containing the IIAT stderr. Returns undefined if the stderr file doesn't exist or there was an error reading it. 
     */
    get error() : string | undefined {
        this.checkJobDirExists();
        const errorFile = this.getJobDir() + "/ERROR.txt";
        if(fs.existsSync(errorFile)) {
            try{
                return fs.readFileSync(errorFile, 'utf8');
            } catch (err) {
                console.error(err);
                return undefined;
            }
        } else return undefined;
    }

    /**
     * The IIAT run time. This reads the current version from the file system, so if the process is still running, it will likely have nothing in it until the process ends.
     * @returns A string containing the IIAT run time. Returns undefined if the time file doesn't exist or there was an error reading it. 
     */
    get time() : string | undefined {
        this.checkJobDirExists();
        const timeFile = this.getJobDir() + "/TIME.txt";
        if(fs.existsSync(timeFile)) {
            try{
                return fs.readFileSync(timeFile, 'utf8');
            } catch (err) {
                console.error(err);
                return undefined;
            }
        } else return undefined;
    }

    /**
     * The compile process stdout. This reads the current version from the file system, so if the process is still running, it reads what has been written so far.
     * @returns A string containing the compile process stdout. Returns undefined if the stdout file doesn't exist or there was an error reading it.
     */
     get compileOutput() : string | undefined {
        this.checkJobDirExists();
        const outputFile = this.getJobDir() + "/COMPILE-OUTPUT.txt";
        if(fs.existsSync(outputFile)) {
            try{
                return fs.readFileSync(outputFile, 'utf8');
            } catch (err) {
                console.error(err);
                return undefined;
            }
        } else return undefined;
    }

    /**
     * The compile process stderr. This reads the current version from the file system, so if the process is still running, it reads what has been written so far.
     * @returns A string containing the compile process stderr. Returns undefined if the stderr file doesn't exist or there was an error reading it. 
     */
     get compileError() : string | undefined {
        this.checkJobDirExists();
        const errorFile = this.getJobDir() + "/COMPILE-ERROR.txt";
        if(fs.existsSync(errorFile)) {
            try{
                return fs.readFileSync(errorFile, 'utf8');
            } catch (err) {
                console.error(err);
                return undefined;
            }
        } else return undefined;
    }

    /**
     * The compile process run time. This reads the current version from the file system, so if the process is still running, it will likely have nothing in it until the process ends.
     * @returns A string containing the compile process run time. Returns undefined if the time file doesn't exist or there was an error reading it. 
     */
     get compileTime() : string | undefined {
        this.checkJobDirExists();
        const timeFile = this.getJobDir() + "/COMPILE-TIME.txt";
        if(fs.existsSync(timeFile)) {
            try{
                return fs.readFileSync(timeFile, 'utf8');
            } catch (err) {
                console.error(err);
                return undefined;
            }
        } else return undefined;
    }

    /**
     * The job's id string (typically a v4 uuid)
     */
    get id() {
        return this._id;
    }

    /**
     * Checks whether the job is currently queued for execution.
     * @returns True if it is currently queued.
     */
    isQueued() : boolean {
        return this._status === JobStatus.Queued;
    }

    /**
     * Checks whether the job is currently running.
     * @returns True if it is currently running.
     */
    isRunning() : boolean {
        return this._status === JobStatus.Running;
    }

    /**
     * Checks whether the job has completed its execution.
     * @returns True if it is complete.
     */
    isCompleted() : boolean {
        return this._status === JobStatus.Completed;
    }

    /**
     * Checks whether the job is in an error state.
     * @returns True if the job is in an error state.
     */
    isInError() : boolean {
        return this._status === JobStatus.Error;
    }

    /**
     * Checks whether the job has been canceled.
     * @returns True if the job is canceled.
     */
    isCanceled() : boolean {
        return this._status === JobStatus.Canceled;
    }

    /**
     * Checks whether the job's working directory exists and creates it if it does not.
     */
    checkJobDirExists() {
        const jobdir = this.getJobDir();
        if(!fs.existsSync(jobdir))
            fs.mkdirSync(jobdir);
    }

    /**
     * Set the error message for the job. This also updates the job status to indicate an error and writes the status file.
     */
    set errorMessage(msg : string | undefined) {
        this._errorMsg = msg;
        if(msg !== undefined)
            this._status = JobStatus.Error;
        this.writeStatusFileFromThis();
    }

    /**
     * Gets the error message. 
     */
    get errorMessage() : string | undefined {
        return this._errorMsg;
    }

    /**
     * Gets the job's working directory.
     * @returns The path to the job's working directory
     */
    getJobDir() {
        if(this._usingTmpDir)
            return pathUtils.resolve(TMP_DIR, this.id);
        else return pathUtils.resolve(WORKSPACE_DIR, this.id);
    }

    /**
     * Cancels the job.
     */
    cancel() {
        const cancelFile = pathUtils.resolve(this.getJobDir(), "CANCEL");
        fs.closeSync(fs.openSync(cancelFile, 'w'));
    }

    /**
     * Deletes the job.
     */
    delete() {
        const deleteFile = pathUtils.resolve(this.getJobDir(), "DELETE");
        fs.closeSync(fs.openSync(deleteFile, "w"));
    }
    
}

export default Job;
