# syntax=docker/dockerfile:1
FROM --platform=linux/amd64 node:16-buster
ARG VERSION=unknown
LABEL maintainer="kjkeefe@illinois.edu"

WORKDIR /usr/local/iiat

ENV PATH="/opt/ghc/bin:${PATH}"
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8
RUN echo "deb http://downloads.haskell.org/debian buster main" >> /etc/apt/sources.list && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys BA3CBA3FFE22B574 && \
    apt-get update && \
    apt-get install -y haskell-platform ghc-8.8.4 cabal-install-3.2 locales libncurses5 && \
    sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && \
    locale-gen && \
    cabal update && \
    cabal install matrix --lib -j1 && \
 #####   cabal install maude --lib && \
    cabal install parsec --lib -j1 && \
    cabal install vector --lib -j1 && \
    cabal install split --lib -j1  

COPY ImplicitInteractionsAnalysisTool /usr/local/iiat/iiat

RUN cd /usr/local/iiat/iiat/lib/maude && \
    cabal configure -j1 && \
    cabal build -j1 && \
    cabal install --lib -j1

COPY web-api /usr/local/iiat/web-api
COPY job-manager /usr/local/iiat/job-manager
COPY bootstrap.sh /usr/local/iiat/
COPY buildAndRun.sh /usr/local/iiat/
RUN chmod +x /usr/local/iiat/bootstrap.sh
COPY gen-lhs/gen-lhs.py /usr/local/iiat/
RUN cd /usr/local/iiat/web-api && \
    npm install && \
    npm run tsc && \
    cd /usr/local/iiat/job-manager && \
    npm install && \
    npm run tsc

EXPOSE 8080
CMD ["/usr/local/iiat/bootstrap.sh"]





